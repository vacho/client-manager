<?php

/* core/modules/system/templates/item-list.html.twig */
class __TwigTemplate_b2b57f7ee5a7ba09c3684f36b63ddeb64aed31c8818607b4568365152a3c30ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 21
        if (((isset($context["items"]) ? $context["items"] : null) || (isset($context["empty"]) ? $context["empty"] : null))) {
            // line 22
            echo "<div class=\"item-list\">";
            // line 23
            if ((isset($context["title"]) ? $context["title"] : null)) {
                // line 24
                echo "<h3>";
                echo twig_render_var((isset($context["title"]) ? $context["title"] : null));
                echo "</h3>";
            }
            // line 26
            if ((isset($context["items"]) ? $context["items"] : null)) {
                // line 27
                echo "<";
                echo twig_render_var((isset($context["list_type"]) ? $context["list_type"] : null));
                echo twig_render_var((isset($context["attributes"]) ? $context["attributes"] : null));
                echo ">";
                // line 28
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 29
                    echo "<li";
                    echo twig_render_var($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "attributes"));
                    echo ">";
                    echo twig_render_var($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "value"));
                    echo "</li>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "</";
                echo twig_render_var((isset($context["list_type"]) ? $context["list_type"] : null));
                echo ">";
            } else {
                // line 33
                echo twig_render_var((isset($context["empty"]) ? $context["empty"] : null));
            }
            // line 35
            echo "</div>";
        }
    }

    public function getTemplateName()
    {
        return "core/modules/system/templates/item-list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 35,  56 => 33,  37 => 28,  71 => 55,  66 => 54,  54 => 50,  49 => 52,  31 => 47,  20 => 44,  42 => 19,  33 => 17,  77 => 58,  74 => 43,  68 => 41,  48 => 48,  45 => 51,  39 => 49,  60 => 53,  57 => 51,  51 => 31,  47 => 47,  35 => 44,  25 => 24,  334 => 201,  328 => 198,  325 => 197,  323 => 196,  320 => 195,  314 => 192,  310 => 191,  306 => 190,  302 => 189,  299 => 188,  297 => 187,  292 => 184,  286 => 181,  282 => 180,  278 => 179,  275 => 178,  273 => 177,  268 => 174,  262 => 171,  259 => 170,  257 => 169,  254 => 168,  248 => 165,  245 => 164,  243 => 163,  237 => 160,  232 => 159,  226 => 156,  223 => 155,  221 => 154,  216 => 153,  210 => 150,  207 => 149,  205 => 148,  200 => 147,  194 => 144,  191 => 143,  189 => 142,  185 => 141,  182 => 140,  176 => 139,  170 => 136,  166 => 134,  160 => 131,  157 => 130,  152 => 128,  141 => 123,  128 => 117,  126 => 116,  121 => 114,  118 => 113,  114 => 111,  108 => 108,  101 => 107,  96 => 105,  83 => 101,  73 => 99,  63 => 53,  50 => 91,  38 => 88,  32 => 27,  29 => 46,  27 => 83,  23 => 23,  26 => 41,  21 => 22,  155 => 129,  149 => 90,  146 => 125,  143 => 124,  137 => 121,  134 => 84,  131 => 118,  125 => 81,  122 => 80,  116 => 77,  113 => 76,  110 => 75,  104 => 73,  102 => 72,  99 => 106,  93 => 104,  90 => 67,  84 => 64,  81 => 46,  79 => 62,  76 => 58,  70 => 56,  67 => 54,  64 => 39,  58 => 53,  55 => 37,  52 => 92,  46 => 47,  43 => 46,  41 => 29,  36 => 31,  30 => 26,  28 => 42,  24 => 16,  19 => 21,);
    }
}
