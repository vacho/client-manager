<?php

/* core/modules/system/templates/field.html.twig */
class __TwigTemplate_5379e3da9d421fea07a790c558bccb3ed2925cbb46badf675a23351d4329d76a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 32
        echo "<div";
        echo twig_render_var((isset($context["attributes"]) ? $context["attributes"] : null));
        echo ">
  ";
        // line 33
        if ((!(isset($context["label_hidden"]) ? $context["label_hidden"] : null))) {
            // line 34
            echo "    <div class=\"field-label\"";
            echo twig_render_var((isset($context["title_attributes"]) ? $context["title_attributes"] : null));
            echo ">";
            echo twig_render_var((isset($context["label"]) ? $context["label"] : null));
            echo ":&nbsp;</div>
  ";
        }
        // line 36
        echo "  <div class=\"field-items\"";
        echo twig_render_var((isset($context["content_attributes"]) ? $context["content_attributes"] : null));
        echo ">
    ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        foreach ($context['_seq'] as $context["delta"] => $context["item"]) {
            // line 38
            echo "      <div class=\"field-item\"";
            echo twig_render_var($this->getAttribute((isset($context["item_attributes"]) ? $context["item_attributes"] : null), (isset($context["delta"]) ? $context["delta"] : null), array(), "array"));
            echo ">";
            echo twig_render_var((isset($context["item"]) ? $context["item"] : null));
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['delta'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "core/modules/system/templates/field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 36,  334 => 201,  328 => 198,  325 => 197,  323 => 196,  320 => 195,  314 => 192,  310 => 191,  306 => 190,  302 => 189,  299 => 188,  297 => 187,  292 => 184,  286 => 181,  282 => 180,  278 => 179,  275 => 178,  273 => 177,  268 => 174,  262 => 171,  259 => 170,  257 => 169,  254 => 168,  248 => 165,  245 => 164,  243 => 163,  237 => 160,  232 => 159,  226 => 156,  223 => 155,  221 => 154,  216 => 153,  210 => 150,  207 => 149,  205 => 148,  200 => 147,  194 => 144,  191 => 143,  189 => 142,  185 => 141,  182 => 140,  176 => 139,  170 => 136,  166 => 134,  160 => 131,  157 => 130,  155 => 129,  152 => 128,  146 => 125,  143 => 124,  141 => 123,  137 => 121,  131 => 118,  128 => 117,  126 => 116,  121 => 114,  118 => 113,  114 => 111,  108 => 108,  101 => 107,  99 => 106,  96 => 105,  93 => 104,  83 => 101,  76 => 100,  73 => 99,  55 => 93,  50 => 91,  43 => 38,  38 => 88,  32 => 85,  29 => 84,  27 => 83,  23 => 82,  71 => 41,  63 => 95,  60 => 38,  54 => 40,  52 => 92,  47 => 34,  41 => 89,  39 => 37,  36 => 30,  26 => 34,  24 => 33,  19 => 32,);
    }
}
