<?php

/* core/modules/system/templates/form-element.html.twig */
class __TwigTemplate_340c55045c412008731a654c7cd287bfa2fb5f15cdd28017562dfd3c69e14b50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 39
        echo "<div";
        echo twig_render_var((isset($context["attributes"]) ? $context["attributes"] : null));
        echo ">
  ";
        // line 40
        if (twig_in_filter((isset($context["label_display"]) ? $context["label_display"] : null), array(0 => "before", 1 => "invisible"))) {
            // line 41
            echo "    ";
            echo twig_render_var((isset($context["label"]) ? $context["label"] : null));
            echo "
  ";
        }
        // line 43
        echo "  ";
        if ((!twig_test_empty((isset($context["prefix"]) ? $context["prefix"] : null)))) {
            // line 44
            echo "    <span class=\"field-prefix\">";
            echo twig_render_var((isset($context["prefix"]) ? $context["prefix"] : null));
            echo "</span>
  ";
        }
        // line 46
        echo "  ";
        echo twig_render_var((isset($context["children"]) ? $context["children"] : null));
        echo "
  ";
        // line 47
        if ((!twig_test_empty((isset($context["suffix"]) ? $context["suffix"] : null)))) {
            // line 48
            echo "    <span class=\"field-suffix\">";
            echo twig_render_var((isset($context["suffix"]) ? $context["suffix"] : null));
            echo "</span>
  ";
        }
        // line 50
        echo "  ";
        if (((isset($context["label_display"]) ? $context["label_display"] : null) == "after")) {
            // line 51
            echo "    ";
            echo twig_render_var((isset($context["label"]) ? $context["label"] : null));
            echo "
  ";
        }
        // line 53
        echo "  ";
        if ($this->getAttribute((isset($context["description"]) ? $context["description"] : null), "content")) {
            // line 54
            echo "    <div";
            echo twig_render_var($this->getAttribute((isset($context["description"]) ? $context["description"] : null), "attributes"));
            echo ">
      ";
            // line 55
            echo twig_render_var($this->getAttribute((isset($context["description"]) ? $context["description"] : null), "content"));
            echo "
    </div>
  ";
        }
        // line 58
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "core/modules/system/templates/form-element.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 55,  66 => 54,  54 => 50,  49 => 52,  31 => 47,  20 => 44,  42 => 19,  33 => 17,  77 => 58,  74 => 43,  68 => 41,  48 => 48,  45 => 51,  39 => 49,  60 => 53,  57 => 51,  51 => 55,  47 => 47,  35 => 44,  25 => 45,  334 => 201,  328 => 198,  325 => 197,  323 => 196,  320 => 195,  314 => 192,  310 => 191,  306 => 190,  302 => 189,  299 => 188,  297 => 187,  292 => 184,  286 => 181,  282 => 180,  278 => 179,  275 => 178,  273 => 177,  268 => 174,  262 => 171,  259 => 170,  257 => 169,  254 => 168,  248 => 165,  245 => 164,  243 => 163,  237 => 160,  232 => 159,  226 => 156,  223 => 155,  221 => 154,  216 => 153,  210 => 150,  207 => 149,  205 => 148,  200 => 147,  194 => 144,  191 => 143,  189 => 142,  185 => 141,  182 => 140,  176 => 139,  170 => 136,  166 => 134,  160 => 131,  157 => 130,  152 => 128,  141 => 123,  128 => 117,  126 => 116,  121 => 114,  118 => 113,  114 => 111,  108 => 108,  101 => 107,  96 => 105,  83 => 101,  73 => 99,  63 => 53,  50 => 91,  38 => 88,  32 => 43,  29 => 46,  27 => 83,  23 => 27,  26 => 41,  21 => 13,  155 => 129,  149 => 90,  146 => 125,  143 => 124,  137 => 121,  134 => 84,  131 => 118,  125 => 81,  122 => 80,  116 => 77,  113 => 76,  110 => 75,  104 => 73,  102 => 72,  99 => 106,  93 => 104,  90 => 67,  84 => 64,  81 => 46,  79 => 62,  76 => 58,  70 => 56,  67 => 54,  64 => 39,  58 => 53,  55 => 37,  52 => 92,  46 => 47,  43 => 46,  41 => 46,  36 => 31,  30 => 28,  28 => 42,  24 => 40,  19 => 39,);
    }
}
