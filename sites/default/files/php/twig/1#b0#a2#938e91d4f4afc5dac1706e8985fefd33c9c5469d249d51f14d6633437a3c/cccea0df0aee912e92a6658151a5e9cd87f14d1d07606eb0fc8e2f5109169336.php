<?php

/* themes/khipuman/templates/page.html.twig */
class __TwigTemplate_b0a2938e91d4f4afc5dac1706e8985fefd33c9c5469d249d51f14d6633437a3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 80
        echo "<div id=\"page-wrapper\"><div id=\"page\">

  <header id=\"header\" class=\"";
        // line 82
        echo twig_render_var((((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null)) ? ("with-secondary-menu") : ("without-secondary-menu")));
        echo "\" role=\"banner\"><div class=\"section clearfix\">
   ";
        // line 83
        if ((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null)) {
            // line 84
            echo "      <nav id=\"secondary-menu\" class=\"navigation\" role=\"navigation\">
        ";
            // line 85
            echo twig_render_var((isset($context["secondary_menu"]) ? $context["secondary_menu"] : null));
            echo "
      </nav> <!-- /#secondary-menu -->
    ";
        }
        // line 88
        echo "
    ";
        // line 89
        if (((isset($context["site_name"]) ? $context["site_name"] : null) || (isset($context["site_slogan"]) ? $context["site_slogan"] : null))) {
            // line 90
            echo "      <div id=\"name-and-slogan\"";
            if (((isset($context["hide_site_name"]) ? $context["hide_site_name"] : null) && (isset($context["hide_site_slogan"]) ? $context["hide_site_slogan"] : null))) {
                echo " class=\"visually-hidden\"";
            }
            echo ">
        ";
            // line 91
            if ((isset($context["site_name"]) ? $context["site_name"] : null)) {
                // line 92
                echo "          ";
                if ((isset($context["title"]) ? $context["title"] : null)) {
                    // line 93
                    echo "            <div id=\"site-name\"";
                    if ((isset($context["hide_site_name"]) ? $context["hide_site_name"] : null)) {
                        echo " class=\"visually-hidden\"";
                    }
                    echo ">
              <strong>
                <a href=\"";
                    // line 95
                    echo twig_render_var((isset($context["front_page"]) ? $context["front_page"] : null));
                    echo "\" title=\"";
                    echo twig_render_var(t("Home"));
                    echo "\" rel=\"home\"><span>";
                    echo twig_render_var((isset($context["site_name"]) ? $context["site_name"] : null));
                    echo "</span></a>
              </strong>
            </div>
          ";
                    // line 99
                    echo "          ";
                } else {
                    // line 100
                    echo "            <h1 id=\"site-name\"";
                    if ((isset($context["hide_site_name"]) ? $context["hide_site_name"] : null)) {
                        echo " class=\"visually-hidden\" ";
                    }
                    echo ">
              <a href=\"";
                    // line 101
                    echo twig_render_var((isset($context["front_page"]) ? $context["front_page"] : null));
                    echo "\" title=\"";
                    echo twig_render_var(t("Home"));
                    echo "\" rel=\"home\"><span>";
                    echo twig_render_var((isset($context["site_name"]) ? $context["site_name"] : null));
                    echo "</span></a>
            </h1>
          ";
                }
                // line 104
                echo "        ";
            }
            // line 105
            echo "
        ";
            // line 106
            if ((isset($context["site_slogan"]) ? $context["site_slogan"] : null)) {
                // line 107
                echo "          <div id=\"site-slogan\"";
                if ((isset($context["hide_site_slogan"]) ? $context["hide_site_slogan"] : null)) {
                    echo " class=\"visually-hidden\"";
                }
                echo ">
            ";
                // line 108
                echo twig_render_var((isset($context["site_slogan"]) ? $context["site_slogan"] : null));
                echo "
          </div>
        ";
            }
            // line 111
            echo "      </div><!-- /#name-and-slogan -->
    ";
        }
        // line 113
        echo "
    ";
        // line 114
        echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header"));
        echo "

    ";
        // line 116
        if ((isset($context["main_menu"]) ? $context["main_menu"] : null)) {
            // line 117
            echo "      <nav id =\"main-menu\" class=\"navigation\" role=\"navigation\">
        ";
            // line 118
            echo twig_render_var((isset($context["main_menu"]) ? $context["main_menu"] : null));
            echo "
      </nav> <!-- /#main-menu -->
    ";
        }
        // line 121
        echo "  </div></header> <!-- /.section, /#header-->

  ";
        // line 123
        if ((isset($context["messages"]) ? $context["messages"] : null)) {
            // line 124
            echo "    <div id=\"messages\"><div class=\"section clearfix\">
      ";
            // line 125
            echo twig_render_var((isset($context["messages"]) ? $context["messages"] : null));
            echo "
    </div></div> <!-- /.section, /#messages -->
  ";
        }
        // line 128
        echo "
  ";
        // line 129
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "featured")) {
            // line 130
            echo "    <aside id=\"featured\"><div class=\"section clearfix\">
      ";
            // line 131
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "featured"));
            echo "
    </div></aside> <!-- /.section, /#featured -->
  ";
        }
        // line 134
        echo "
  <div id=\"main-wrapper\" class=\"clearfix\"><div id=\"main\" class=\"clearfix\">
    ";
        // line 136
        echo twig_render_var((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null));
        echo "

    <main id=\"content\" class=\"column\" role=\"main\"><section class=\"section\">
      ";
        // line 139
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted")) {
            echo "<div id=\"highlighted\">";
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted"));
            echo "</div>";
        }
        // line 140
        echo "      <a id=\"main-content\"></a>
      ";
        // line 141
        echo twig_render_var((isset($context["title_prefix"]) ? $context["title_prefix"] : null));
        echo "
        ";
        // line 142
        if ((isset($context["title"]) ? $context["title"] : null)) {
            // line 143
            echo "          <h1 class=\"title\" id=\"page-title\">
            ";
            // line 144
            echo twig_render_var((isset($context["title"]) ? $context["title"] : null));
            echo "
          </h1>
        ";
        }
        // line 147
        echo "      ";
        echo twig_render_var((isset($context["title_suffix"]) ? $context["title_suffix"] : null));
        echo "
        ";
        // line 148
        if ((isset($context["tabs"]) ? $context["tabs"] : null)) {
            // line 149
            echo "          <nav class=\"tabs\" role=\"navigation\">
            ";
            // line 150
            echo twig_render_var((isset($context["tabs"]) ? $context["tabs"] : null));
            echo "
          </nav>
        ";
        }
        // line 153
        echo "      ";
        echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help"));
        echo "
        ";
        // line 154
        if ((isset($context["action_links"]) ? $context["action_links"] : null)) {
            // line 155
            echo "          <ul class=\"action-links\">
            ";
            // line 156
            echo twig_render_var((isset($context["action_links"]) ? $context["action_links"] : null));
            echo "
          </ul>
        ";
        }
        // line 159
        echo "      ";
        echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content"));
        echo "
      ";
        // line 160
        echo twig_render_var((isset($context["feed_icons"]) ? $context["feed_icons"] : null));
        echo "
    </section></main> <!-- /.section, /#content -->

    ";
        // line 163
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first")) {
            // line 164
            echo "      <div id=\"sidebar-first\" class=\"column sidebar\"><aside class=\"section\">
        ";
            // line 165
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first"));
            echo "
      </aside></div><!-- /.section, /#sidebar-first -->
    ";
        }
        // line 168
        echo "
    ";
        // line 169
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second")) {
            // line 170
            echo "      <div id=\"sidebar-second\" class=\"column sidebar\"><aside class=\"section\">
        ";
            // line 171
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second"));
            echo "
      </aside></div><!-- /.section, /#sidebar-second -->
    ";
        }
        // line 174
        echo "
  </div></div><!-- /#main, /#main-wrapper -->

  ";
        // line 177
        if ((($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_first") || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_middle")) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_last"))) {
            // line 178
            echo "    <div id=\"triptych-wrapper\"><aside id=\"triptych\" class=\"clearfix\">
      ";
            // line 179
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_first"));
            echo "
      ";
            // line 180
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_middle"));
            echo "
      ";
            // line 181
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "triptych_last"));
            echo "
    </aside></div><!-- /#triptych, /#triptych-wrapper -->
  ";
        }
        // line 184
        echo "
  <div id=\"footer-wrapper\"><footer class=\"section\">

    ";
        // line 187
        if (((($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_firstcolumn") || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_secondcolumn")) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_thirdcolumn")) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_fourthcolumn"))) {
            // line 188
            echo "      <div id=\"footer-columns\" class=\"clearfix\">
        ";
            // line 189
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_firstcolumn"));
            echo "
        ";
            // line 190
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_secondcolumn"));
            echo "
        ";
            // line 191
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_thirdcolumn"));
            echo "
        ";
            // line 192
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_fourthcolumn"));
            echo "
      </div><!-- /#footer-columns -->
    ";
        }
        // line 195
        echo "
    ";
        // line 196
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer")) {
            // line 197
            echo "      <div id=\"footer\" role=\"contentinfo\" class=\"clearfix\">
        ";
            // line 198
            echo twig_render_var($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer"));
            echo "
      </div> <!-- /#footer -->
   ";
        }
        // line 201
        echo "
  </footer></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
";
    }

    public function getTemplateName()
    {
        return "themes/khipuman/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 201,  328 => 198,  325 => 197,  323 => 196,  320 => 195,  314 => 192,  310 => 191,  306 => 190,  302 => 189,  299 => 188,  297 => 187,  292 => 184,  286 => 181,  282 => 180,  278 => 179,  275 => 178,  273 => 177,  268 => 174,  262 => 171,  259 => 170,  257 => 169,  254 => 168,  248 => 165,  245 => 164,  243 => 163,  237 => 160,  232 => 159,  226 => 156,  223 => 155,  221 => 154,  216 => 153,  210 => 150,  207 => 149,  205 => 148,  200 => 147,  194 => 144,  191 => 143,  189 => 142,  185 => 141,  182 => 140,  176 => 139,  170 => 136,  166 => 134,  160 => 131,  157 => 130,  152 => 128,  141 => 123,  128 => 117,  126 => 116,  121 => 114,  118 => 113,  114 => 111,  108 => 108,  101 => 107,  96 => 105,  83 => 101,  73 => 99,  63 => 95,  50 => 91,  38 => 88,  32 => 85,  29 => 84,  27 => 83,  23 => 82,  26 => 25,  21 => 24,  155 => 129,  149 => 90,  146 => 125,  143 => 124,  137 => 121,  134 => 84,  131 => 118,  125 => 81,  122 => 80,  116 => 77,  113 => 76,  110 => 75,  104 => 73,  102 => 72,  99 => 106,  93 => 104,  90 => 67,  84 => 64,  81 => 63,  79 => 62,  76 => 100,  70 => 58,  67 => 57,  64 => 56,  58 => 53,  55 => 93,  52 => 92,  46 => 48,  43 => 90,  41 => 89,  36 => 45,  30 => 43,  28 => 42,  24 => 41,  19 => 80,);
    }
}
