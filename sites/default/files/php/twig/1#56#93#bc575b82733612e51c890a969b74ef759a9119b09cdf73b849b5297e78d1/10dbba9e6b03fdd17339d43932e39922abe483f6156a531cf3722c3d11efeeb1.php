<?php

/* core/modules/system/templates/breadcrumb.html.twig */
class __TwigTemplate_5693bc575b82733612e51c890a969b74ef759a9119b09cdf73b849b5297e78d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 12
        if ((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null)) {
            // line 13
            echo "  <nav class=\"breadcrumb\" role=\"navigation\">
    <h2 class=\"visually-hidden\">";
            // line 14
            echo twig_render_var(t("You are here"));
            echo "</h2>
    <ol>
    ";
            // line 16
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 17
                echo "      <li>";
                echo twig_render_var((isset($context["item"]) ? $context["item"] : null));
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "    </ol>
  </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "core/modules/system/templates/breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 19,  33 => 17,  77 => 44,  74 => 43,  68 => 41,  48 => 35,  45 => 34,  39 => 32,  60 => 52,  57 => 50,  51 => 36,  47 => 47,  35 => 43,  25 => 41,  334 => 201,  328 => 198,  325 => 197,  323 => 196,  320 => 195,  314 => 192,  310 => 191,  306 => 190,  302 => 189,  299 => 188,  297 => 187,  292 => 184,  286 => 181,  282 => 180,  278 => 179,  275 => 178,  273 => 177,  268 => 174,  262 => 171,  259 => 170,  257 => 169,  254 => 168,  248 => 165,  245 => 164,  243 => 163,  237 => 160,  232 => 159,  226 => 156,  223 => 155,  221 => 154,  216 => 153,  210 => 150,  207 => 149,  205 => 148,  200 => 147,  194 => 144,  191 => 143,  189 => 142,  185 => 141,  182 => 140,  176 => 139,  170 => 136,  166 => 134,  160 => 131,  157 => 130,  152 => 128,  141 => 123,  128 => 117,  126 => 116,  121 => 114,  118 => 113,  114 => 111,  108 => 108,  101 => 107,  96 => 105,  83 => 101,  73 => 99,  63 => 95,  50 => 91,  38 => 88,  32 => 29,  29 => 16,  27 => 83,  23 => 27,  26 => 25,  21 => 13,  155 => 129,  149 => 90,  146 => 125,  143 => 124,  137 => 121,  134 => 84,  131 => 118,  125 => 81,  122 => 80,  116 => 77,  113 => 76,  110 => 75,  104 => 73,  102 => 72,  99 => 106,  93 => 104,  90 => 67,  84 => 64,  81 => 46,  79 => 62,  76 => 58,  70 => 56,  67 => 54,  64 => 39,  58 => 53,  55 => 37,  52 => 92,  46 => 48,  43 => 46,  41 => 89,  36 => 31,  30 => 28,  28 => 42,  24 => 14,  19 => 12,);
    }
}
