/**
     * @file
     * Defines Javascript behaviors for the node module.
     */
(function(a){a.fn.validCampo=function(b){a(this).on({keypress:function(a){
	var c=a.which,d=a.keyCode, e=String.fromCharCode(c).toLowerCase(), f=b;(-1!=f.indexOf(e)||9==d||37!=c&&37==d||39==d&&39!=c||8==d||46==d&&46!=c)&&161!=c||a.preventDefault()}})}})(jQuery);
 
 var normalize = (function() {
  var from = " ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç.#'\\ºª!|$%&()=?¿¡[];,:{}+<>€¬~@/",
      to   = "-AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }
      return ret.join( '' );
  }
 
})();
 
 
(function ($) {
  $(document).ready(function(){
/*
*  Validaciones de campos
*/		
		$('#edit-phone').validCampo('0123456789-');
		$('#edit-phonebranch').validCampo('0123456789-');
		$('#edit-nroemployees').validCampo('0123456789');
		$('#edit-nrobranches').validCampo('0123456789');
		$('#edit-c-registration').validCampo('0123456789');
		$('#edit-nit').validCampo('0123456789');
  	$('#edit-domain').validCampo('abcdefghijklmnopqrstuvwxyz-_0123456789');
    $("#edit-re-password-pass1").attr("placeholder", "_Clave");
    $("#edit-re-password-pass2").attr("placeholder", "_Confirmar clave");

/*
* Controla la fecha cada cuanto se pagara a la renta
*/
   $("#edit-nit").keyup(function() {
      dato = this.value
      dig = parseInt(dato[dato.length-1]) + 13
      $('#edit-informationday').val(dig)
    }); 
		
   $("#edit-submit").attr("disabled", true);
   $("#edit-submit").addClass("disabled");
   
   
/***************   BOTON NEXT DEL DATA PERSON   ***************/

   $("#dataPerson .next a" ).click(function() {
      $("#dataCompany").css('display','inherit')
      $("#dataPerson").css('display','none')
      $("#navegation li.step1").removeClass("active");
      $("#navegation li.step2").addClass("active");
      return false;
   }); 

/***************   BOTON NEXT AND BACK DEL DATA COMPANY   ***************/
    $("#dataCompany .next a" ).click(function() {
      var dato = $("#edit-branch").val().toLowerCase();
      dato = normalize(dato);
		  $("#edit-domain").val(dato);

      $("#dataTributary").css('display','inherit')
      $("#dataCompany").css('display','none')
      $("#navegation li.step2").removeClass("active");
      $("#navegation li.step3").addClass("active");
     return false;
    }); 

    $("#dataCompany .back a" ).click(function() {
      $("#dataPerson").css('display','inherit')
      $("#dataCompany").css('display','none')
      $("#navegation li.step2").removeClass("active");
      $("#navegation li.step1").addClass("active");
      return false;
    }); 

/***************   BOTON NEXT AND BACK DEL DATA TRIBUTARY   ***************/
    $("#dataTributary .next a" ).click(function() {
      $("#dataUser").css('display','inherit')
      $("#dataTributary").css('display','none')
      $("#edit-submit").css('display','inherit')
      $("#navegation li.step3").removeClass("active");
      $("#navegation li.step4").addClass("active");
      return false;
    }); 

    $("#dataTributary .back a" ).click(function() {
      $("#dataCompany").css('display','inherit')
      $("#dataTributary").css('display','none')
      $("#navegation li.step3").removeClass("active");
      $("#navegation li.step2").addClass("active");
      return false;
    }); 

/***************   BOTON NEXT AND BACK DEL DATA USER   ***************/
    $("#dataUser .back a" ).click(function() {
      $("#dataTributary").css('display','inherit')
      $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
      $("#navegation li.step4").removeClass("active");
      $("#navegation li.step3").addClass("active");
      return false;
    }); 

/***************   BOTONES DE NAVEGACION   ***************/

    $("#navegation .step1 a" ).click(function() {
	   $("#navegation li.step1").addClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','inherit')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','none')
     $("#edit-submit").css('display','none')
     return false;
    // return false;
    }); 
    $("#navegation .step2 a" ).click(function() {
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").addClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','inherit')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
     return false;
    }); 
    $("#navegation .step3 a" ).click(function() {
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").addClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','inherit')
     $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
     return false;
    }); 

    $("#navegation .step4 a" ).click(function() {
     var dato = $("#edit-branch").val().toLowerCase();
     dato = normalize(dato);
		 $("#edit-domain").val(dato);
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").addClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','inherit')
      $("#edit-submit").css('display','inherit')
     return false;
    }); 
  /* 
  *
  */

   if($("#edit-newsletter-0").is(":checked")) {
      $("#edit-submit").attr("disabled", false);
     $("#edit-submit").removeClass("disabled");
   }


   $("#edit-newsletter-0").click(function() {
    if($(this).is(":checked")) {
      $("#edit-submit").attr("disabled", false);
     $("#edit-submit").removeClass("disabled");
   }else{
     $("#edit-submit").attr("disabled", true);
     $("#edit-submit").addClass("disabled");
   }   
   });


   $("#edit-submit").click(function() {
	 //  men = "Completa este campo :";
			
	   
		 /*
		 *  Campos del bloque "Informacion de negocios"
		 */

	   if($("#edit-branch").val().length == 0  || $("#edit-phonebranch").val().length == 0  || $("#edit-email").val().length == 0  || $("#edit-country").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }

	   if($("#edit-department").val().length == 0  || $("#edit-address").val().length == 0  || $("#edit-activity").val().length == 0  || $("#edit-entry").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }
     
	   if($("#edit-nroemployees").val().length == 0  || $("#edit-nrobranches").val().length == 0  || $("#edit-currency").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }

		 /*
		 *  Campos del bloque "Informacion personal"
		 */
		 
		 if($("#edit-name").val().length == 0  || $("#edit-lastname").val().length == 0  )
	   { $("#navegation .step1 a").trigger('click'); }
   });
/* ******************************************
   popup para ver politicas y servicios
  *********************************************/
//     $( "#politicas" ).dialog({ autoOpen: false });
	$(".police a").click(function() {
		$("#popup").css('display', 'inherit')
			return false;
		});
	});
   
   
   
})(jQuery);;
Drupal.locale = {"strings":{"":{"Changed":"Cambiado","An AJAX HTTP error occurred.":"Hubo un error HTTP AJAX.","HTTP Result Code: !status":"C\u00f3digo de Resultado HTTP: !status","An AJAX HTTP request terminated abnormally.":"Una solicitud HTTP de AJAX termin\u00f3 de manera anormal.","Debugging information follows.":"A continuaci\u00f3n se detalla la informaci\u00f3n de depuraci\u00f3n.","Path: !uri":"Ruta: !uri","StatusText: !statusText":"StatusText: !statusText","ResponseText: !responseText":"ResponseText: !responseText","ReadyState: !readyState":"ReadyState: !readyState","Please wait...":"Espere, por favor...","Edit":"Editar","Save":"Guardar","Open":"Abrir","Add":"Agregar","OK":"OK","Close":"Cerrar","Show":"Mostrar","Select all rows in this table":"Seleccionar todas las filas de esta tabla","Deselect all rows in this table":"Quitar la selecci\u00f3n a todas las filas de esta tabla","Extend":"Extender","Hide":"Ocultar","Quick edit":"Edici\u00f3n r\u00e1pida","Remove group":"Eliminar grupo","@label":"@label","Drag to re-order":"Arrastre para reordenar","Changes made in this table will not be saved until the form is submitted.":"Los cambios realizados en esta tabla no se guardar\u00e1n hasta que se env\u00ede el formulario","Discard changes":"\u00bfDescartar cambios?","Show description":"Mostrar descripci\u00f3n","Collapse":"Plegar","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"El archivo seleccionado %filename no puede ser subido. Solo se permiten archivos con las siguientes extensiones: %extensions.","Re-order rows by numerical weight instead of dragging.":"Reordenar las filas por peso num\u00e9rico en lugar de arrastrar.","Show row weights":"Mostrar pesos de la fila","Hide row weights":"Ocultar pesos de la fila","Apply (all displays)":"Aplicar (todas las presentaciones)","Apply (this display)":"Aplicar (esta presentaci\u00f3n)","Revert to default":"Volver al valor inicial","Hide description":"Esconder descripci\u00f3n","You have unsaved changes":"Usted tiene cambios no guaradados","Show all columns":"Mostrar todas las columnas","Hide unimportant columns":"Ocultar columnas no importantes","Show table cells that were hidden to make the table fit within a small screen.":"Mostrar celdas de tablas que estaban ocultas para que la tabla se ajuste a una pantalla peque\u00f1a.","List additional actions":"Lista adicional de acciones","Horizontal orientation":"Orientaci\u00f3n horizontal","Vertical orientation":"Orientaci\u00f3n vertical","Tray orientation changed to @orientation.":"La orientaci\u00f3n de la bandeja se ha cambiado a @orientation.","You have unsaved changes.":"Tiene cambios sin guardar.","@action @title configuration options":"@action @title opciones de configuraci\u00f3n","Tabbing is no longer constrained by the Contextual module.":"Tabulando (Tabbing) ya no se ver\u00e1 limitado por el m\u00f3dulo Contextual (Contextual).","Tabbing is constrained to a set of @contextualsCount and the edit mode toggle.":"La tabulaci\u00f3n est\u00e1 restringida a un conjunto de @contextualsCount y al cambio a modo de edici\u00f3n.","Press the esc key to exit.":"Presionar tecla esc para salir.","@count contextual link\u0003@count contextual links":"@count enlace contextual\u0003@count enlaces contextuales","!tour_item of !total":"!tour_item de !total","End tour":"Final del tour"}},"pluralFormula":{"1":0,"default":1}};;

(function ($) {
  $(document).ready(function(){
     $("#popup .cerrar").click(function() {
       //$("#politicas").dialog(); 
			 $("#popup").css('display', 'none')
       return false;
		 });

   });
   
})(jQuery);;
