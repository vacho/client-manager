<?php
/**
*  @file 
*   Contains Drupal\creator.
*/


namespace Drupal\createuser;

use Drupal\Core\Form\FormInterface;
use Drupal\Component\Utility\String;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\ConnectionNotDefinedException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\UserSession;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\block\Entity\Block;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;

class createUser extends ControllerBase implements FormInterface {

public function getFormID() {
  return 'createuser';
}

 function buildForm(array $form, array &$form_state) {

  $us=arg(1);
  $res = db_query('select name, password, email from k_usuario where uid='.$us)->fetchAssoc();
  if(isset($res['name']) && strlen($res['name']) > 0){
    $F_user = array(
      'name' => $res['name'],
      'mail' => $res['email'],
      'pass' => $res['password'],
    );

    $res1 = db_query("SELECT count(*) FROM users WHERE name='".$res['name']."' and mail='".$res['email']."';")->fetchField();
    if($res1==0) {
      $usuario = $this->drupalCreateUser($F_user);
      $res = db_query('delete from k_usuario where uid='.$us);
    } else {
      drupal_set_message("El usuario ya existe");
    }
  
  }
  return $this->redirect('<front>');
}


public function validateForm(array &$form, array &$form_state) {
  }


public function submitForm(array &$form, array &$form_state) {

}


public function drupalCreateUser(array $edit = array(), $us = NULL) {
    $edit += array(
      'status' => 1,
      'roles'  => array('Authenticated user', 'cliente'),
    );

    $account = entity_create('user', $edit);
    $account->save();
    if (!$account->id()) {
      return FALSE;
   }
    // Add the raw password so that we can log in as this user.
    $account->pass_raw = $edit['pass'];
    return $account;
  }


}