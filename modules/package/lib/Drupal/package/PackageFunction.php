<?php

/**
 * @file
 * Contains \Drupal\package\PackageFunction.
 */

namespace Drupal\package;

use Drupal\Core\Controller\ControllerInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PackageFunction extends ControllerBase{
  function validateSend(){
    if(isset($_POST['form-id'])){
      $sql='DELETE FROM package_module WHERE package='.$_GET['package'].'; ';
      for($i=0; $i < $_POST['sizeTable']; $i++){
        if(isset($_POST['checkbox-'.$i]) && $_POST['checkbox-'.$i]=='on'){
          $sql .= 'INSERT INTO package_module VALUES('.$_GET['package'].' , '.$_POST['che-'.$i].'); ';
        }
      }
      $cod = db_query($sql);;
      return true;
    }else 
      return false;
  }


  function ListModules(){
    if($this->validateSend())
      return $this->redirect('package.package-list');    
    $getURL=$_GET['package'];
    $res = db_query('SELECT name FROM package WHERE pid='.$getURL);
    foreach ($res as $dat)
      $namePakage = $dat->name;
    $sqlcheck='SELECT mid, name FROM `modentity` WHERE mid in(SELECT modules FROM package_module WHERE package='.$getURL.')';
    $res = db_query($sqlcheck);
    $i=0;
    foreach ($res as $dat) {
      $aux[$i][0] = 1;
      $aux[$i][1] = $dat->mid;
      $aux[$i][2] = $dat->name;
      $i++;
    }

    $sqllibre='SELECT mid, name FROM `modentity` WHERE activado = 1 and mid not in(SELECT modules FROM package_module WHERE package='.$getURL.')';
    $res = db_query($sqllibre);
    foreach ($res as $dat) {
      $aux[$i][0] = 0;
      $aux[$i][1] = $dat->mid;
      $aux[$i][2] = $dat->name;
      $i++;
    }

    $content = '<h1 class="title" id="page-title">'.t('Modules for package').' : '.$namePakage.'</h1> <div class="view-content">';
    $content .= '<form class="add-modules-form package-form form-updated-processed" action="/package/add-modules?package='.$getURL.'" method="post" id="add-module-form" accept-charset="UTF-8" data-drupal-form-field="edit-check,edit-submit">';
    $content .= '<input type="hidden" name="sizeTable" value="'.$i.'"><div><table class="views-table views-view-table cols-3">';
    if(isset($aux)){
      $content .= $this->headTable(array(t('Enabled')."/".t('Disabled'), t('Cod'), t('Module')));
      $content .= $this->contentTable($aux, array(t('Enabled')."/".t('Disabled'), t('Cod'), t('Module')));
    }
    $content .= '</table><input type="hidden" name="form-id" value="add-modules-form"></div>';
    $content .= '<div class="form-action form-wrapper" id="edit-action"> <input type="submit" id="edit-submit" name="op" value="'.t('Save').'" class="button button-primary form-submit"></div>';
    
    $lista['contentForm'] = array(
      '#markup' => $content.'</form></div>',
    );
    return $lista;
  }

  function headTable($datos = array()){
    $tam = count($datos);
    $headH ='<thead><tr>';
    $head = "";
    for($i = 0; $i < $tam; $i++){
      $head .= '<th class="views-field views-field-'.$datos[$i].'" scope="col">';
      $head .= $datos[$i];
      $head .= '</th>';
    }
    $headH .= $head.'</thead>';
    return $headH;
  }

  function contentTable($datos = array(), $tit = array()){
    $tamv = count($datos);
    $tamh = count($tit);
    $contentT ='<tbody>';
    $content = '';
    for($i = 0; $i < $tamv; $i++){
      $firs="";
      $last="";
      $fila="even";
      if($i % 2 == 0)
        $fila="odd";
      if($i == 0)
        $firs=" views-row-first";
      if($i == ($tamv-1))
        $last=" views-row-last";
      $content .= '<tr class="'.$fila.$firs.$last.'">';
      $cuerpo='';
      for($j=0; $j < $tamh; $j++)  {
        $chek='';
        if($j==0){
          if($datos[$i][0]==1)
            $chek=' checked="checked"';
          $datos[$i][$j]='<input type="checkbox" name="checkbox-'.$i.'" id="checkbox-'.$i.'" '.$chek.'"/>';
          $datos[$i][$j] .= '<input type="hidden" name="che-'.$i.'" value="'.$datos[$i][1].'"/>';
        }
        $cuerpo .= '<td class="views-field views-field-'.$tit[$j].'" headers="view-'.$tit[$j].'-table-column">'.$datos[$i][$j].'</td>';
      }
      $content .= $cuerpo.'</tr>';
    }
    $contentT .= $content.'</tbody>';
    return $contentT;
  }
}
