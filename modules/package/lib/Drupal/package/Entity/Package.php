<?php
/**
 * @file
 * Definition of Drupal\package\Entity\package.
 */

namespace Drupal\package\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Package entity class.
 *
 * @ContentEntityType(
 *   id = "package",
 *   label = @Translation("Entity package"),
 *   controllers = {
 *     "view_builder" = "Drupal\package\PackageViewBuilder",
 *     "form" = {
 *       "edit" = "Drupal\package\PackageFormController",
 *       "add" = "Drupal\package\PackageFormController",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationController"
 *   },
 *   base_table = "package",
 *   admin_permission = "administer display modes",
 *   fieldable = TRUE,
 *   field_cache = FALSE,
 *   entity_keys = {
 *     "id" = "pid",
 *     "uuid" = "uuid",
 *     "label" = "name"
 *   },
 * )
 */
class Package extends ContentEntityBase {

  public function id() {
    return $this->get('pid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name->value;
  }

  /**
   * {@inheritdoc}
   */
/*  public function getCode() {
    return $this->code->value;
  }
*/

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['pid'] = FieldDefinition::create('integer')
      ->setLabel('ID')
      ->setDescription(t('The ID of the Package Person entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = FieldDefinition::create('uuid')
      ->setLabel('UUID')
      ->setDescription(t('The UUID of the Package entity.'))
      ->setReadOnly(TRUE);

    $fields['langcode'] = FieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of the package Person entity.'));

    $fields['name'] = FieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the package Person entity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    // @todo: Add allowed values validation.
    $fields['type'] = FieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The bundle of the package entity.'))
      ->setRequired(TRUE);

/*    $fields['code'] = FieldDefinition::create('string')
      ->setLabel(t('code package'))
      ->setDescription(t('The code package entity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));
*/
    return $fields;
  }

}




