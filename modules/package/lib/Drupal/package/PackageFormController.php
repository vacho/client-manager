<?php
/**
 * @file
 * Definition of Drupal\package\PackageFormController.
 */

namespace Drupal\package;

//use Drupal\Core\Entity\ContentEntityFormController;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;

/**
 * Form controller for the package entity edit forms.
 */
//class PackageFormController extends ContentEntityFormController {
class PackageFormController extends ContentEntityForm {

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'). ' ('. t('package').')',
      '#default_value' => $entity->name->value,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => -10,
    );

/*    $form['user_id'] = array(
      '#type' => 'textfield',
      '#title' => 'UID',
      '#default_value' => $entity->user_id->target_id,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => -10,
    );
    $form['code'] = array(
      '#type' => 'textfield',
      '#title' => t('code'),
      '#default_value' => $entity->code->value,
      '#size' => 60,
      '#maxlength' => 128,
      '#weight' => -8,
    );
*/
/*
    $form['langcode'] = array(
      '#title' => t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->id,
      '#languages' => Language::STATE_ALL,
    );

*/
    return $form;
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::submit().
   */
  public function submit(array $form, array &$form_state) {
    // Build the entity object from the submitted values.
    $entity = parent::submit($form, $form_state);
    $form_state['redirect'] = 'package-list';

    return $entity;
  }


  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $entity = $this->entity;
    $entity->save();
  }
}

