<?php

/**
 * @file
 * Contains \Drupal\package\packageViewBuilder.
 */

namespace Drupal\package;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
/**
 *
 * @see \Drupal\package
 */
class PackageViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildContent(array $entities, array $displays, $view_mode, $langcode = NULL) {
    parent::buildContent($entities, $displays, $view_mode, $langcode);

    foreach ($entities as $entity) {
       $entity->content['package']['name'] = array(
        '#markup' => $entity->getName(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
/*      $entity->content['package']['code'] = array(
        '#markup' => $entity->getCode(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
*/
    }
  }

}
