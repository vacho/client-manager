<?php

/*
 * @file
 * Provide views data for package.module.
 */


/**
 *  Implements hook_views_data.
 */

function package_views_data() {
  $data['package']['table']['group']= t('package entities');
  $data['package']['table']['entity type'] = 'package';

  $data['package']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Table for package entities'),
    'help' => t('Whatever you are looking for'),
    'weight' => -10,
  );

  $data['package']['pid'] =array(
    'title' => t('Package ID'),
    'help' => t('The id of package'),
    'field' => array(
      'id' => 'standard',
    ),
  );

  $data['package']['name'] = array(
    'title' => t('Name package'),
    'help'=> t('Text field name'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['package']['type'] = array(
    'title' => t('Entity type'),
    'filter' => array(
      'id' => 'string',
    ),
  );
/*  $data['package']['code'] = array(
    'title' => t('Package entity'),
    'help'=> t('text field for code'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );
*/  return $data;
}
