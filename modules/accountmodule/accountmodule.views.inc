<?php

/*
 * @file
 * Provide views data for accountmodule.module.
 */


/**
 *  Implements hook_views_data.
 */

function accountmodule_views_data() {
  $data['accountmodule']['table']['group']= t('account entities');

  $data['accountmodule']['table']['entity type'] = 'accountmodule';

  $data['accountmodule']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Table for account entities'),
    'help' => t('whatever you are looking for'),
    'weight' => -10,
  );

  $data['accountmodule']['type'] = array(
    'title' => t('Entity type'),
    'filter' => array(
      'id' => 'string',
    ),
  );


  $data['accountmodule']['uid'] =array(
    'title' => 'uid',
    'help' => t('The id of dä thing'),
    'field' => array(
      'id' => 'standard',
    ),
  );

  $data['accountmodule']['pid'] = array(
    'title' => t('person id'),
    'help'=> t('text field'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['accountmodule']['rid'] = array(
    'title' => t('Entry id'),
    'help'=> t('text field'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['accountmodule']['name'] = array(
    'title' => t('Name of the entity'),
    'help'=> t('text field'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['accountmodule']['account'] = array(
    'title' => t('User account'),
    'help'=> t('text field'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );
  $data['accountmodule']['entry'] = array(
    'title' => t('entry of main activi'),
    'help'=> t('text field'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );
  $data['accountmodule']['active'] = array(
    'title' => t('Active'),
    'help'=> t('user active'),
    'field' => array(
      'id' => 'standard',
    ),
  );
  return $data;
}
