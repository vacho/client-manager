<?php

/**
 * @file
 * Contains \Drupal\accountmodule\AccountmoduleViewBuilder.
 */

namespace Drupal\accountmodule;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
/**
 * Defines an entity view builder for a account entity.
 *
 * @see \Drupal\accountmodule
 */
class AccountmoduleViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildContent(array $entities, array $displays, $view_mode, $langcode = NULL) {
    parent::buildContent($entities, $displays, $view_mode, $langcode);
    foreach ($entities as $entity) {
       $entity->content['accountmodule']['rid'] = array(
        '#markup' => $entity->getRid(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
       $entity->content['accountmodule']['pid'] = array(
        '#markup' => $entity->getPid(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
       $entity->content['accountmodule']['name'] = array(
        '#markup' => $entity->getName(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
      $entity->content['accountmodule']['account'] = array(
        '#markup' => $entity->getAccount(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
      $entity->content['accountmodule']['entry'] = array(
        '#markup' => $entity->getEntry(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );

       $entity->content['accountmodule']['active'] = array(
        '#markup' => $entity->getActive(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }
  }

}
