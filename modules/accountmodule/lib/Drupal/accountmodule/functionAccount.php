<?php

/**
 * @file
 * Contains \Drupal\accountmodule\functionAccount.
 */

namespace Drupal\accountmodule;
use Drupal\Core\Controller\ControllerBase;

/**
 * Defines an entity view builder for a account entity.
 *
 * @see \Drupal\accountmodule
 */
class functionAccount extends ControllerBase {

  public function activeOnOff(){
    $sql='select status from users where uid='.arg(1);
    $num= db_query('select status from users where uid='.arg(1))->fetchField();
    if($num == 0)
      $num = 1;
    else
      $num = 0;
    $res=db_query('update users set status = '.$num.' where uid='.arg(1));
    return $this->redirect('accountmodule.account-list');
  }

/*  function validateSend(){
    if(isset($_POST['form-id']) && $_POST['form-id'] == 'add-modules-form'){
      $sql='DELETE FROM package_module WHERE package='.$_GET['package'].'; ';
      for($i=0; $i < $_POST['sizeTable']; $i++){
        if(isset($_POST['checkbox-'.$i]) && $_POST['checkbox-'.$i]=='on'){
          $sql .= 'INSERT INTO package_module VALUES('.$_GET['package'].' , '.$_POST['che-'.$i].'); ';
        }
      }
      $cod = db_query($sql);;
      return true;
    }else 
      return false;
  }
*/
  function listPackage(){

    /*******
       AL HACER CLICK EN SAVE DE PACKAGE
    ********/

    if(isset($_POST['option']) && $_POST['option'] == 'package'){
      if($_POST['field-package']==0)
        drupal_set_message(t('Select a package'), 'error');
      else {
        $res = db_query('DELETE FROM account_package WHERE person='.arg(1).' and entry='.arg(2));
        $res = db_query('INSERT INTO account_package VALUES('.arg(1).', '.arg(2).', '.$_POST['field-package'].')');
        // ****  Modules for account
        $res = db_query('DELETE FROM account_module WHERE person='.arg(1).' and entry='.arg(2));
        $res= db_query('SELECT modules FROM package_module WHERE package='.$_POST['field-package'])->fetchCol();
        $listmodule="0";
        for($i=0; $i < count($res);$i++){
          $addsql = db_query('INSERT INTO account_module VALUES('.arg(1).', '.arg(2).', '.$res[$i].')');
          $listmodule .= ", " . $res[$i];
        }
        $rescopy = db_query("select m.name as modulo, nd.field_domain_value as dominio from modentity as m, node__field_domain as nd where nd.entity_id=". arg(2) ." and m.mid in( " . $listmodule . " );")->fetchAll();
        $listamodulos="";
        foreach ($rescopy as $fila) {
             $listamodulos .= $fila->modulo.";";
//          $origen=$_SERVER['DOCUMENT_ROOT']."/myModules/". $fila->modulo ."/";
//          $destino="C:/xampp/htdocs/". $fila->dominio ."/modules/". $fila->modulo ."/";
//          $destino = "/home/koalasof/public_html/".$fila->dominio."/modules/".$fila->modulo ."/";
 //         $this->full_copy($origen, $destino);
        }

        $txt = $_SERVER['DOCUMENT_ROOT'];
        $txt = explode("/", $txt);
        $listamodulos .= "<->".$txt[count($txt) - 1];

        
        $this->crearArchivo($fila->dominio, $listamodulos);
        drupal_set_message(t('Keep the packet is successfully'));
      }
    
    }
    
    /*******
       AL HACER CLICK EN SAVE DE MODULES
    ********/
    
    if(isset($_POST['form-id']) && $_POST['form-id'] == 'add-modules-form'){
       $tam = $_POST['sizeTable'];
       $res = db_query('DELETE FROM account_module WHERE person='.arg(1).' and entry='.arg(2));
       $listmodule="0";
       for($i=0; $i<$tam;$i++){
          if(isset($_POST['checkbox-'.$i]) && $_POST['checkbox-'.$i]== 'on')  {
            $addsql = db_query('INSERT INTO account_module VALUES('.arg(1).', '.arg(2).', '.$_POST['che-'.$i].')');
            $listmodule .= ", " . $_POST['che-'.$i];
          }
          
            $rescopy = db_query("select m.name as modulo, nd.field_domain_value as dominio from modentity as m, node__field_domain as nd where nd.entity_id=". arg(2) ." and m.mid in( " . $listmodule . " );")->fetchAll();
        $listamodulos = "";
        foreach ($rescopy as $fila) {
          $listamodulos .= $fila->modulo.";";
//          $origen=$_SERVER['DOCUMENT_ROOT']."/myModules/". $fila->modulo ."/";
//          $destino = "/home/koalasof/public_html/".$fila->dominio."/modules/".$fila->modulo ."/";
          
//          $this->full_copy($origen, $destino);
        }
//        $listamodulos .= $_SERVER['DOCUMENT_ROOT'];
        $txt = $_SERVER['DOCUMENT_ROOT'];
        $txt = explode("/", $txt);
        $listamodulos .= "<->".$txt[count($txt) - 1];
        
        $this->crearArchivo($fila->dominio, $listamodulos);

       }
       drupal_set_message(t('Keep the modules successfully'));
    }
  
    
    
    $res = db_query('select title, type from node_field_data where nid in('.arg(1).','.arg(2).')')->fetchAll();
    $nom=$rub="";
    foreach ($res as $fila) {
      if($fila->type == 'persona')
        $nom = $fila->title;
      else
        $rub = $fila->title;
    }
    $nameAccount = $nom." - ".$rub;
    $idpackage = db_query('SELECT package FROM account_package WHERE person='.arg(1).' and entry='.arg(2))->fetchField();
    $selected = ' selected="selected"';
    $selected = '';
    $options="";
    $res = db_query('SELECT pid, name FROM package')->fetchAll();
    foreach ($res as $fila) {
      $selected="";
      if($fila->pid == $idpackage)
        $selected = ' selected="selected"';
      $options .= '<option value='.$fila->pid.$selected.'>'.$fila->name.'</option>';
    }


    $lista = '<div class="form-item form-type-select form-item-field-package">';
    $lista .= '<label for="edit-field-package">'.t('List Package').'</label>';
    $lista .= '<select id="edit-field-package" name="field-package" class="form-select">';
    $lista .= '<option value=0>'.t('Select Package').'</option>'.$options;
    $lista .= '</select></div>';

    
    $form1 = '<h1 class="title" id="page-title">'.t('Package for account')." : ".$nameAccount.'</h1> <div class="view-content" id="package-account">';
    $form1 .= '<form class="add-modules-form account-form form-updated-processed" action="/account-package-module/'.arg(1).'/'.arg(2).'" method="post" id="package-module-form" accept-charset="UTF-8" data-drupal-form-field="field-package,edit-submit">';
    $form1 .= '<input type="hidden" name="option" value="package">';
    $form1 .= $lista.'<div class="form-action form-wrapper" id="edit-action"> <input type="submit" id="edit-submit" name="op" value="'.t('Save').'" class="button button-primary form-submit"></div>';
    $form1 .= '</form></div>';
    $form1 .= $this->ListModules();
    return array('#markup' => $form1);
  
  }
  
  
  
  function crearArchivo($dominio = '', $dato = ''){
   // $ruta=$_SERVER['DOCUMENT_ROOT']."/myModules";
    $ruta= "/home/khipucom/public_html/".$dominio ."/modules/".$fila->modulo ."/";
    if (!$file = fopen($ruta . "/listaModulos.txt", 'w')) {
      echo "No se puede abrir el archivo ($filesetting)";
      exit;
    }
    fwrite($file, $dato);
    @fclose($file);
  }

  function ListModules(){
    $sqlcheck='SELECT mid, name FROM `modentity` WHERE mid in(SELECT modules FROM account_module WHERE person='.arg(1).' and entry='.arg(2).')';
    $res = db_query($sqlcheck);
    $i=0;
    foreach ($res as $dat) {
      $aux[$i][0] = 1;
      $aux[$i][1] = $dat->mid;
      $aux[$i][2] = $dat->name;
      $i++;
    }

    $sqllibre='SELECT mid, name FROM `modentity` WHERE activado = 1 and mid not in(SELECT modules FROM account_module WHERE person='.arg(1).' and entry='.arg(2).')';
    $res = db_query($sqllibre);
    foreach ($res as $dat) {
      $aux[$i][0] = 0;
      $aux[$i][1] = $dat->mid;
      $aux[$i][2] = $dat->name;
      $i++;
    }

    $content = '<h1 class="title" id="page-title">'.t('List Modules').'</h1> <div class="view-content">';
    $content .= '<form class="add-modules-form package-form form-updated-processed" action="/account-package-module/'.arg(1).'/'.arg(2).'" method="post" id="add-module-form" accept-charset="UTF-8" data-drupal-form-field="edit-check,edit-submit">';
    $content .= '<input type="hidden" name="sizeTable" value="'.$i.'"><div><table class="views-table views-view-table cols-3">';
    if(isset($aux)){
      $content .= $this->headTable(array(t('Check'), t('Cod'), t('Module')));
      $content .= $this->contentTable($aux, array(t('Check'), t('Cod'), t('Module')));
    }
    $content .= '</table><input type="hidden" name="form-id" value="add-modules-form"></div>';
    $content .= '<div class="form-action form-wrapper" id="edit-action"> <input type="submit" id="edit-submit" name="op" value="'.t('Save').'" class="button button-primary form-submit"></div>';
    $lista = $content.'</form></div>';
    return $lista;
  }

  function headTable($datos = array()){
    $tam = count($datos);
    $headH ='<thead><tr>';
    $head = "";
    for($i = 0; $i < $tam; $i++){
      $head .= '<th class="views-field views-field-'.$datos[$i].'" scope="col">';
      $head .= $datos[$i];
      $head .= '</th>';
    }
    $headH .= $head.'</thead>';
    return $headH;
  }

  function contentTable($datos = array(), $tit = array()){
    $tamv = count($datos);
    $tamh = count($tit);
    $contentT ='<tbody>';
    $content = '';
    for($i = 0; $i < $tamv; $i++){
      $firs="";
      $last="";
      $fila="even";
      if($i % 2 == 0)
        $fila="odd";
      if($i == 0)
        $firs=" views-row-first";
      if($i == ($tamv-1))
        $last=" views-row-last";
      $content .= '<tr class="'.$fila.$firs.$last.'">';
      $cuerpo='';
      for($j=0; $j < $tamh; $j++)  {
        $chek='';
        if($j==0){
          if($datos[$i][0]==1)
            $chek=' checked="checked"';
          $datos[$i][$j]='<input type="checkbox" name="checkbox-'.$i.'" id="checkbox-'.$i.'" '.$chek.'"/>';
          $datos[$i][$j] .= '<input type="hidden" name="che-'.$i.'" value="'.$datos[$i][1].'"/>';
        }
        $cuerpo .= '<td class="views-field views-field-'.$tit[$j].'" headers="view-'.$tit[$j].'-table-column">'.$datos[$i][$j].'</td>';
      }
      $content .= $cuerpo.'</tr>';
    }
    $contentT .= $content.'</tbody>';
    return $contentT;
  }



function full_copy( $source, $target ) { 
   if ( is_dir( $source ) ) { 
      @mkdir( $target );
      $d = dir( $source );
      while ( FALSE !== ( $entry = $d->read() ) ) { 
        if ( $entry == '.' || $entry == '..' ) { 
           continue; 
        } 
        $Entry = $source . '/' . $entry; 
        if ( is_dir( $Entry ) ) { 
          $this->full_copy( $Entry, $target . '/' . $entry ); 
          continue; 
        } copy( $Entry, $target . '/' . $entry ); 
      } 
      $d->close(); 
    } else { 
       copy( $source, $target ); 
     } 
}

}
  