<?php
/**
 * @file
 * Definition of Drupal\accountmodule\Entity\accountmodule.
 */

namespace Drupal\accountmodule\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the accountmodule entity class.
 *
 * @ContentEntityType(
 *   id = "accountmodule",
 *   label = @Translation("Entity account"),
 *   controllers = {
 *     "view_builder" = "Drupal\accountmodule\AccountmoduleViewBuilder",
 *     "translation" = "Drupal\content_translation\ContentTranslationController"
 *   },
 *   base_table = "accountmodule",
 *   admin_permission = "administer display modes",
 *   fieldable = TRUE,
 *   field_cache = FALSE,
 *   entity_keys = {
 *     "id" = "uid",
 *     "label" = "name"
 *   },
 * )
 */
class Accountmodule extends ContentEntityBase {

  public function changeActive(){
		$act="jajajaja ";
		return array('#markup'=>$act);
	}
	
	public function id() {
    return $this->get('uid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccount() {
    return $this->account->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntry() {
    return $this->entry->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPid() {
    return $this->pid->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getRid() {
    return $this->rid->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getActive() {
    return $this->active->value;
  }



  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['uid'] = FieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the account Person entity.'))
      ->setReadOnly(TRUE);


    $fields['pid'] = FieldDefinition::create('string')
      ->setLabel(t('person id'))
      ->setDescription(t('The person ID.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    $fields['rid'] = FieldDefinition::create('string')
      ->setLabel(t('entry id'))
      ->setDescription(t('The entry ID.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    $fields['account'] = FieldDefinition::create('string')
      ->setLabel(t('user account'))
      ->setDescription(t('The user account.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    $fields['entry'] = FieldDefinition::create('string')
      ->setLabel(t('Entry'))
      ->setDescription(t('entry of main activity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    $fields['name'] = FieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the account Person entity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    $fields['active'] = FieldDefinition::create('string')
      ->setLabel(t('Active user'))
      ->setDescription(t('The entry ID.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    return $fields;
  }
	
}




