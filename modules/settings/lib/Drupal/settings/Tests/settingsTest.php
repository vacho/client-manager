<?php

/**
 * @file
 * Tests for settings.module.
 */

namespace Drupal\settings\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the settings module.
 */
class settingsTest extends WebTestBase {

  public static function getInfo() {
    return array(
      'name' => 'settings functionality',
      'description' => 'Test Unit for module settings.',
      'group' => 'Other',
    );
  }

  function setUp() {
    parent::setUp();
  }

  /**
   * Tests settings functionality.
   */
  function testsettings() {
    //Check that the basic functions of module settings.
    $this->assertEqual(TRUE, TRUE, 'Test Unit Generated via Console.');
  }

}
