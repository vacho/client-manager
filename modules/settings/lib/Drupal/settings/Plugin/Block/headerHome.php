<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability status.
 *
 * @Block(
 *   id = "header_home",
 *   admin_label = @Translation("Header Home"),
 *   category = @Translation("System"),
 * )
 */

class HeaderHome extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => '0',
    );
  }

  public function build() {
    $build = array();
    $build['#markup'] = "<div class='saludo' id='saludo'>";
    $build['#markup'] .= "<span class='bienvenidoa'>".t('Welcome to')."</span>";
    $build['#markup'] .= "<img alt=".t('Home')." height ='84' width='234' src='themes/khipuman/logoCenter.png'>";
    //$build['#markup'] .= "<div class='slogan'>".t('Smart Accounting in just one clic.')."</div>";
    $build['#markup'] .= "</div>";

    return $build;
  }
}