<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability status.
 *
 * @Block(
 *   id = "new_user",
 *   admin_label = @Translation("You are new"),
 *   category = @Translation("System")
 * )
 */
class NewUser extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => 'visible',
    );
  }

  public function build() {
    $build = array();
    $build['#markup'] = "<div class='eresNuevo' id='eresNuevo'>";
    $build['#markup'] .= "<p>".t("Create an account to enjoy the system")."</p>";
//    $build['#markup'] .= "<p class='centro'><a href='/clientregister/add' title='".t('Create account')."'><input class='button form-submit' id='edit-submit' name='cc' type='submit' value='".t('Create account')."'></a></p>";
    $build['#markup'] .= '<p class="boton_block"><input type="button" title="'.t('Create account').'" value="'.t('Create account').'" onClick="window.location.href=\'/clientregister/add\' "></p>';
    $build['#markup'] .= "</div>"; 

    return $build;

  }
}