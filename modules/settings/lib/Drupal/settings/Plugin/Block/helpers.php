<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability status.
 *
 * @Block(
 *   id = "helpers",
 *   admin_label = @Translation("Helpers"),
 *   category = @Translation("System"),
 * )
 */

class Helpers extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => '0',
    );
  }

  public function build() {
    $build = array();
    $build['#markup'] = "<div class='helpers' id='helpers'>";
    $build['#markup'] .= "<a href='/' title=".t("Frequently questions")." rel=".t("Frequently questions")." id='frecuently-asks'>";
    $build['#markup'] .= "".t('Frequently questions')."";
    $build['#markup'] .= "</a>";
    $build['#markup'] .= " | ";
    $build['#markup'] .= "<a href='/' title=".t("Help")." rel=".t("Help")." id='help'>";
    $build['#markup'] .= "".t('Help')."";
    $build['#markup'] .= "</a></div>";

    return $build;
  }
}