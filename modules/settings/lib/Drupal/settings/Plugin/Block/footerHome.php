<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability status.
 *
 * @Block(
 *   id = "footer_home",
 *   admin_label = @Translation("Footer Home"),
 *   category = @Translation("System"),
 * )
 */

class FooterHome extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => '0',
    );
  }

  public function build() {
    $build = array();
    $build['#markup'] = "<div class='footerHome' id='footerHome'>";
    $build['#markup'] .= "<p class='contacto'>info@khipu.com.bo<br>591.4.4559169</p>";
    $build['#markup'] .= "<p>".t('Income')." | ".t('Expenses')." | ".t('Loans')." | ".t('Reports')."</p>";
    $build['#markup'] .= "</div>";

    return $build;
  }
}
