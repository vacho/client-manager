<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability 
 *
 * @Block(
 *   id = "header_user",
 *   admin_label = @Translation("Header User"),
 *   category = @Translation("System"),
 * )
 */

class HeaderUser extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => '0',
    );
  }

  public function build() {
    $build = array();
    $build['#markup'] = "<div class='headerUser' id='headerUser'>";
    $build['#markup'] .= "<a href='/' title=".t("Home")." rel=".t("home")." id='logo'>";
    $build['#markup'] .= "<img alt=".t('Home')." src='themes/khipuman/logo.png'>";
    $build['#markup'] .= "</a></div>";

    return $build;
  }
}
