<?php
namespace Drupal\settings\Plugin\Block;

use Drupal\block\BlockBase;

/**
 *  * Reports on hugability status.
 *
 * @Block(
 *   id = "user_access",
 *   admin_label = @Translation("User Access"),
 *   category = @Translation("System"),
 * )
 */

class UserAccess extends BlockBase {

  public function defaultConfiguration(){
    return array(
      'enabled' => 1,
      'label_display' => '0',
    );
  }

  public function build() {
    global $user;
 
    $build = array();
    $build['#markup'] = "<div class='userAccess' id='userAccess'>";
    $build['#markup'] .= "".t('Welcome')." ".$user->name;
    $build['#markup'] .= "</br>";
    $build['#markup'] .= "<a href='/' title=".t("Configurations")." rel=".t("Configurations")." id='configurations'>";
    $build['#markup'] .= "".t('Configurations')."";
    $build['#markup'] .= "</a>";
    $build['#markup'] .= " | ";
    $build['#markup'] .= "<a href='/user/logout' title=".t("Logout")." rel=".t("Logout")." id='logout'>";
    $build['#markup'] .= "".t('logout')."";
    $build['#markup'] .= "</a>";
    $build['#markup'] .= "</div>";

    return $build;
  }
}
