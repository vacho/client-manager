<?php

namespace Drupal\accountmenu\Controller;

use Drupal\Core\Controller\ControllerBase;

class DefaultController extends ControllerBase {

  /**
   * initMenu
   * @return string
   */
  public function startMenu() {
    return "Wellcome to Account Menu";
  }
}
