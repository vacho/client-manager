<?php

namespace Drupal\clientregister;

use Drupal\Core\Form\FormInterface;
use Drupal\Component\Utility\String;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\ConnectionNotDefinedException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\UserSession;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\block\Entity\Block;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\creator\creator;
use Drupal\popup\Popup;

class Add implements FormInterface {

protected $loggedInUser = FALSE;

function getFormID() {
   return 'Add';
}

function buildForm(array $form, array &$form_state) {
    $mensaje = new Popup();
        
    $form['#attached']['js'] = array(
      drupal_get_path('module', 'clientregister') . '/js/script.js',
    );    
    $form['#attached']['css'] = array(
      drupal_get_path('module', 'clientregister') . '/css/stilos.css',
      drupal_get_path('module', 'clientregister') . '/css/horizontal.css',
      drupal_get_path('module', 'clientregister') . '/css/vertical.css',
    );
/*
     $sql="select title, body_value from node_field_data as nf, node__body as nb, node__field_activo as na where nf.nid=nb.entity_id and nf.nid=na.entity_id and na.field_activo_value=1 and nf.type='policy_terms' order by nf.nid DESC Limit 1;";
   $terminos = db_query($sql)->fetchAssoc();

    $popup = '<div id="popup" style="display:none"><div id="politicas"><div class="cuerpo"><table width="100%" border="0" cellpadding="0" cellspacing="0">';
    $popup .= '<tr class="titulo"><td width="*">'.$terminos['title'].'</td><td width="30px"><div class="cerrar">X</div>';
    $popup .= '</td></tr><tr><td colspan="2"><div class="content"><div>'.$terminos['body_value'].'</div> </div></td></tr></table></div></div></div>';
//    print $popup;
    $form['police'] = array(
      '#markup' => $popup,
    );
*/
    $form['loader'] = array(
      '#markup' => '<div id="loader" style="display:none;"><div class="imagen"><img src="/modules/clientregister/image/loading.gif" border="0" /></div> <div class="mensaje"><b>'.t('At this time your information is being processed, this can take several seconds. <br>Wait a moment please.').' </b></div></div> ',
    );


/**************  PERSON DATA *******************/



    $form['div_person'] = array(
      '#markup' => '<div id="addData"><div id="content_body">  <div id="dataPerson"><div class="title_data"><p> '.t('Personal information is useful to export financial reports.').' </p></div><div class="content"><div class="block_center">',
    );      

    $form['name'] = array(
     '#type' => 'textfield',
     '#title' => t('Name'),
     '#required' => TRUE,
     '#placeholder' => '_'.t('Name'),
      '#maxlength'=> 50,
    );
    $form['lastName'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Last Name'),
      '#maxlength'=> 50,
    );

    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => t('Phone'),
      '#placeholder' => '_'.t('Phone'),
      '#maxlength'=> 15,
    );

    $form['step1'] = array(
      '#markup' => '<div class="obligado"><span>(*)</span>  '.t('Required fields').'</div></div></div> <div class="ant_next">  <div class="next step1">'.l(t('Next'), '/javascript()').'</div> </div> </div> ',
    );

/**************  BUSINES DATA  *******************/

    $form['div_company'] = array(
        '#markup' => '<div id="dataCompany"> <div class="title_data"><p> '.t('Business information optimizes the system settings for activity.').' </p></div><div class="content"> <div class="block_left">',
    );      
   
    $form['branch'] = array(
      '#type' => 'textfield',
      '#title' => t('Company'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Company'),
      '#maxlength'=> 60,
    );

    $form['phonebranch'] = array(
      '#type' => 'tel',
      '#title' => t('Phone'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Phone'),
      '#maxlength'=> 15,
    );

    $form['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Email'),
    );

/*    $form['country'] = array(
      '#markup' => $this->selectDataList( array(
      'name' => "country",
      'title' => t('Country'),
      'valores' => array('Bolivia'),
      'default_value' => 'Bolivia',
      'required' => TRUE,
      'placeholder' => '_'.t('Country'),
      )),
    );      
*/
    $form['country'] = array(

      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => array(
        '' => '_'.t('Country'),
        'Bolivia' => 'Bolivia',
      ),
      '#default_value' => 'Bolivia',
      '#required' => TRUE,
      '#placeholder' => '_'.t('Country'),
    );      

/*    $form['department'] = array(
      '#markup' => $this->selectDataList( array(
        'name' => "department",
        'title' => t('Department'),
        'valores' => array('Beni', 'Chuquisaca', 'Cochabamba', 'La Paz', 'Oruro', 'Pando', 'Potosí', 'Santa Cruz', 'Tarija'),
        'required' => TRUE,
        'placeholder' => '_'.t('Department'),
        'default_value' => 'Cochabamba',
        )
      ), 
    );      
*/
    $form['department'] = array(
      '#type' => 'select',
      '#name' => 'Department',
      '#title' => t('Department'),
      '#required' => TRUE,
      '#options' => array(
        '' => '_'.t('Department'),
        'Beni' => 'Beni',
        'Chuquisaca' => 'Chuquisaca',
        'Cochabamba' => 'Cochabamba',
        'La Paz' => 'La Paz',
        'Oruro' => 'Oruro',
        'Pando' => 'Pando',
        'Potosí' => 'Potosí',
        'Santa Cruz' => 'Santa Cruz',
        'Tarija' => 'Tarija',
      ),
      '#default_value' => 'Cochabamba',
      '#placeholder' => '_'.t('Department'),
    );      



    $form['address'] = array(
      '#type' => 'textarea',
      '#title' => t('Main branch address'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Main branch address'),
    );
    
    $form['bloque'] = array(
      '#markup' => '</div><div class="block_right">',
    );

/*    $form['Activity'] = array(
      '#markup' => $this->selectDataList( array(
      'name' => "Activity",
      'title' => t('Main Activity'),
      'valores' => array(t('Commercial'), t('Services')),
      'required' => TRUE,
      'placeholder' => '_'.t('Main Activity'),
      )),
    );      
*/
    $form['Activity'] = array(
      '#type' => 'select',
      '#name' => 'Activity',
      '#title' => t('Activity'),
      '#required' => TRUE,
      '#options' => array(
        '' => '_'.t('Main Activity'),
        'Commercial' => t('Commercial'),
        'Services' => t('Services'),
      ),
      '#default_value' => '_'.t('Main Activity'),
      '#placeholder' => '_'.t('Main Activity'),
    );      
/*
    $form['Entry'] = array(
      '#markup' => $this->selectDataList( array(
      'name' => "Entry",
      'title' => t('Entry'),
      'valores' => array(t('Shop'),t('Professional services')),
      'required' => TRUE,
      'placeholder' => '_'.t('Entry'),
      )),
    );      
*/

    $form['Entry'] = array(
      '#type' => 'select',
      '#name' => 'Entry',
      '#title' => t('Entry'),
      '#required' => TRUE,
      '#options' => array(
        '' => '_'.t('Entry'),
        'Professional services' => t('Professional services'),
        'Shop' => t('Shop'),
      ),
      '#default_value' => '_'.t('Entry'),
      '#placeholder' => '_'.t('Entry'),
    );      


    $form['nroemployees'] = array(
      '#type' => 'textfield',
      '#title' => 'Nº '.t('Employees'),
      '#required' => TRUE,
      '#placeholder' => '_ Nº '.t('Employees'),
      '#maxlength'=> 5,
    );

    $form['nrobranches'] = array(
      '#type' => 'textfield',
      '#title' => 'Nº '.t('Branches'),
      '#required' => TRUE,
      '#placeholder' => '_ Nº '.t('Branches'),
      '#maxlength'=> 5,
    );

    $form['c_registration'] = array(
      '#type' => 'textfield',
      '#title' => t('Commercial registration'),
      '#placeholder' => '_'.t('Commercial registration'),
      '#maxlength'=> 15,
    );

/*    $form['currency'] = array(
      '#markup' => $this->selectDataList( array(
      'name' => "currency",
      'title' => t('Currency'),
      'valores' => array('Boliviano', 'Dolar Americano', 'Euro', 'UFV'),
      'default_value' => 'Boliviano',
      'required' => TRUE,
      'placeholder' => '_'.t('Currency'),
      )),
    );      
*/

    $form['currency'] = array(
      '#type' => 'select',
      '#name' => 'currency',
      '#title' => t('Currency'),
      '#required' => TRUE,
      '#options' => array(
        '' => '_'.t('Currency'),
        'Bs' => 'Boliviano',
        'Sus' => 'Dolar Americano',
        'Euro' => 'Euro',
        'UFV' => 'UFV',
      ),
      '#default_value' => '',
      '#placeholder' => '_'.t('Currency'),
    );      



   $form['step2'] = array(
      '#markup' => '</div> </div> <div class="ant_next"><div class="back">'.l(t('Back'), '#').'</div><div class="next">'.l(t('Next'), '#').'</div></div></div>',
    );
/**************  Domain DATA  *******************/
/*
    $form['div_domain'] = array(
        '#markup' => '<div class="block_left">',
    );      

    $form['domain'] = array(
      '#type' => 'textfield',
      '#title' => t('NIT'),
      '#placeholder' => '_'.t('NIT'),
    );

    $form['stepa'] = array(
      '#markup' => '</div>',
    );


/**************  TRIBUTARY DATA  *******************/

    $form['div_tributary'] = array(
        '#markup' => '<div id="dataTributary"> <div class="title_data"><p> '.t('Tax information is useful for generating reports for income.').' </p></div> <div class="content"><div class="block_center">',
    );      

    $form['nit'] = array(
      '#type' => 'textfield',
      '#title' => t('NIT'),
      '#placeholder' => '_'.t('NIT'),
      '#maxlength'=> 15,
    );
    $form['datebegin'] = array(
      '#type' => 'date',
      '#title' => t('Accounting period start date'),
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#date_year_range' => '2014:+3',
      '#default_value' => date('Y-m-d'), //format_date('962424000', 'custom', 'd-m-Y'), //'1/'.date('m').'/'.date('Y'),
      '#description' => t('Accounting period start date'),
      '#required' => TRUE,
    );

    $form['informationday'] = array(
      '#type' => 'number',
      '#title' => t('Day Information report income'),
      '#default_value' => 1, //format_date('962424000', 'custom', 'Y-m-d H:i:s O'), 
      '#description' => t('Day of the month which shall submit its report income (1 to 31).'),
      '#required' => TRUE,
    );

    $form['step3'] = array(
      '#markup' => '</div></div> <div class="ant_next"><div class="back">'.l(t('Back'), '#').'</div><div class="next">'.l(t('Next'), '#').'</div></div></div>',
    );

/**************  USER DATA  *******************/

    $form['div_user'] = array(
        '#markup' => '<div id="dataUser"> <div class="title_data"><p> '.t('User information allows you to have a secure account.').' </p></div> <div class="content"><div class="block_left">',
    );      

    $form['domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Domain'),
      '#maxlength' => 20,
      '#required' => TRUE,
      '#placeholder' => '_'.t('Domain Name'),
      '#prefix' => '<div class="title_domain">'.t('Domain Name').'</div>',
      '#suffix' => '<div class="suffix_domain">.khipu.com.bo</div><div id="domain_control">'.t('Does not support special characters.').'</div>',
//      '#suffix' => '<div class="suffix_domain">.khipu.com.bo</div> <div id="domain_control">'.t('Does not support special characters.').'</div>',
    );

    $dato_us="us"; $dato_pass="pass";
    $res=db_query("select max(entity_id) from node__field_user")->fetchField();
    if($res > 0){
      $dato_us .= (10000 + $res + 1);
      $dato_pass = (10000 + $res + 1).$dato_pass;
      $num = $res + 1;
    } else {
      $dato_us .= '10001';
      $dato_pass = '10001'.$dato_pass;
      $num = 1;
    }
    
    $form['db_domain'] = array(
      '#type' => 'hidden',
      '#value' => $num,
    );

    $form['us_domain'] = array(
      '#type' => 'hidden',
      '#value' => $dato_us,
    );
    $form['pass_domain'] = array(
      '#type' => 'hidden',
      '#value' => $dato_pass,
    );

    $form['stepa'] = array(
      '#markup' => '</div><div class="block_right">',
    );

    $form['email_user'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('User email'),
    );

/*    $form['password'] = array(
      '#type' => 'password',
      '#title' => t('Current password'),
      '#size' => 25,
      '#access' => !empty($protected_values),
      '#description' => 'Password user',
      '#attributes' => array('autocomplete' => 'off'),
      '#placeholder' => '_'.t('Password'),
    );

    $form['re_password'] = array(
      '#type' => 'password_confirm',
      '#size' => 25,
      '#description' => t('Provide a password for the new account in both fields.'),
      '#required' => TRUE,
      '#placeholder' => '_'.t('Confirm Password'),
    );
*/
    $cuerpo = array(
      'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida convallis ligula non pharetra. Aliquam ornare suscipit tincidunt. Duis eu ligula eget leo porta adipiscing. Morbi lectus libero, porta eget sagittis malesuada, varius non lectus. Nunc tristique vulputate ligula. Vestibulum volutpat vitae nulla quis facilisis. Pellentesque libero dolor, semper quis lacus quis, adipiscing semper ante. Suspendisse a dolor eros. Pellentesque sodales, libero ac accumsan tempor, metus sapien pellentesque odio, ut laoreet libero mi sed nisl. Aliquam in eros vulputate nulla ornare sollicitudin. Pellentesque mollis turpis sed fringilla volutpat.</p>

<p>Vivamus porta laoreet enim, eget dignissim ante. Aenean interdum condimentum imperdiet. Aliquam tempor urna felis. Nulla facilisi. Morbi eget tortor sit amet turpis lobortis vehicula eget non lectus. Etiam dolor erat, elementum eu justo sed, ornare faucibus metus. Nulla vehicula, arcu eu lobortis pretium, ligula diam tincidunt dui, non tincidunt mi neque ut urna. Aliquam iaculis pretium nunc vitae consectetur.</p>

<p>Suspendisse lobortis enim ut lacinia blandit. Fusce quis libero dui. Fusce varius nisl et nisl ornare porttitor. Nam a interdum diam. Cras fringilla mi ac nunc lacinia, a auctor felis tristique. Morbi vitae risus elementum est fringilla vulputate quis auctor enim. Nam non ultrices nisl. Vestibulum eu euismod nisl. Aliquam suscipit et tellus et hendrerit. Pellentesque pretium sem sit amet euismod egestas. Nulla eu diam quis sem volutpat eleifend ut non nulla. Duis condimentum viverra nisl sit amet eleifend. In non venenatis purus, eu blandit leo. Cras eu faucibus est, sit amet pretium elit. Curabitur a ornare elit, facilisis convallis eros. Aliquam at velit pellentesque, gravida arcu at, aliquet mi.</p>',
      'format' => 'full_html',
    );
  
  $F_Policy = array(
    'title' => "Políticas y términos",
    'body' => array($cuerpo),
    'field_activo' => 1,
    'type' => 'policy_terms',
  );

   $num= db_query("SELECT count(*) FROM node__field_activo WHERE bundle = 'policy_terms'")->fetchField();
   $terminos="";
   if($num == 0)
      $nodoter = $this->drupalCreateNode($F_Policy);


     $sql="select title, body_value from node_field_data as nf, node__body as nb, node__field_activo as na where nf.nid=nb.entity_id and nf.nid=na.entity_id and na.field_activo_value=1 and nf.type='policy_terms' order by nf.nid DESC Limit 1;";
   $terminos = db_query($sql)->fetchAssoc();

/*    $popup = '<div id="popup" style="display:none"><div id="politicas"><div class="cuerpo"><table width="100%" border="0" cellpadding="0" cellspacing="0">';
    $popup .= '<tr class="titulo"><td width="*">'.$terminos['title'].'</td><td width="30px"><div class="cerrar">X</div>';
    $popup .= '</td></tr><tr><td colspan="2"><div class="content"><div>'.$terminos['body_value'].'</div> </div></td></tr></table></div></div></div>';
*/
    
    $form['police'] = array(
//      '#markup' => $popup.'<div class="police"><a id="apolice" href="#">'.t('See policy and terms of service.').'</a></div>',
      '#markup' => $mensaje->message_popup($terminos['title'], $terminos['body_value']).'<div class="police"><a id="apolice" href="#">'.t('See policy and terms of service.').'</a></div>',
/*      '#markup' => '<div id="politicas" title="'.$terminos['title'].'" style="display:none;">'.$terminos['body_value'].'</div>
    <div class="police"><a id="apolice" href="#">'.t('See policy and terms of service.').'</a></div>',
  //  <div class="police">'.l(t('See policy and terms of service.'), '/politicas').'</div>', 
 */   );
   
    $form['newsletter'] = array( 
      '#type' => 'checkboxes',
      '#title' => '',
      '#options' => array(t('I agree with the policy and terms of service.')),
    );

/**************  END DATA USER  *******************/

    $form['send'] = array(
      '#markup' => '</div></div> <div class="ant_next"><div class="back">'.l(t('Back'), '#').'</div></div></div></div>',
    );

/**************  END SUBMIT  *******************/
      $form['div_navegation'] = array(
        '#markup' => '<div id="navegation">',
      );      

    $form['steps'] = array(
      '#markup' => '<ul><li class="step1 active">'.l(t('Personal Information'), '#').'</li><li class="step2">'.l(t('Business Information'), '#').'</li><li class="step3">'.l(t('Tributary Information'), '#').'</li><li class="step4">'.l(t('User'), '#').'</li></ul></div></div>',
    );


/**************  END NAVEGATION  *******************/

    $form['actions'] = array('#type' => 'actions');

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Register'),
    );

      $form['close'] = array(
        '#markup' => '</div">',
      );      

    return $form;
  }

/* 
*   funcion para crear un DATALIST con formato de drupal 
*/

  function selectDataList ($lista = array()){
    $placeholder="";
    $value = "";
    $name = $lista['name'];
    if(isset($lista['placeholder']))
       $placeholder=$lista['placeholder'];
    if(isset($lista['default_value']))
     $value = $lista['default_value'];
    $txt = '<div class="form-item form-type-textfield form-item-'.$name.'">';
    $txt .= '<label for="edit-'.$name.'">'.$lista['title'];
    if(isset($lista['required']) && $lista['required'])
      $txt .='<span class="form-required" aria-hidden="true">*</span>';
    $txt .='</label>';

    $txt .= '<input type="text" list="'.$name.'" value="'.$value.'" name="'.$name.'" id="edit-'.$name.'" placeholder="'.$placeholder.'"';
    if(isset($lista['required']) && $lista['required'])
      $txt .=' class="form-text required" required="required" aria-required="true"';
    $txt .= '><datalist id="'.$name.'">';
    for ($i=0; $i < count($lista['valores']); $i++)
      $txt .= '<option value="'.$lista['valores'][$i].'">';
    $txt .= '</datalist>';
      $txt .='</div>';
    return $txt;
  }


/* ********************************************************************
   Genera un nuevo codigo para persona
***********************************************************************/

  function newCodPersonBranch($tipo='per'){
    $cod="";
    $cod = db_query("SELECT max( field_codigo_value ) FROM node__field_codigo WHERE field_codigo_value LIKE '". $tipo ."%'")->fetchField();
   if(strlen($cod)>0){
      $aux = explode('-', $cod);
      $cod = $aux[1] + 1;
      $cod = $tipo."-".$cod;
   }
    else
    $cod = $tipo."-1";
    return $cod;
  }

/* ********************************************************************
  valida el formulario
***********************************************************************/


  function validateForm(array &$form, array &$form_state) {
   $comercial = db_query("SELECT count(*) FROM node__field_matricula_comercial WHERE field_matricula_comercial_value = '".$form_state['values']['c_registration']."';")->fetchField();
   if($comercial > 0 )   
      form_set_error('Error',  $form_state, t('The registration of commerce')." : " . $form_state['values']['c_registration'] . " " . t('already been registered').". ".t('Please change this field.'));

   $NIT = db_query("SELECT count(*) FROM node__field_nit WHERE field_nit_value = '".$form_state['values']['nit']."';")->fetchField();
   if($NIT > 0 )   
      form_set_error('Error',  $form_state, t('The NIT')." : " . $form_state['values']['nit'] . " " .t('already been registered').". ".t('Please change this field.'));

   $v_user = db_query("SELECT count(*) FROM users WHERE name = '".$form_state['values']['email_user']."';")->fetchField();
   if($v_user > 0 )   
      form_set_error('Error',  $form_state, t('The user account with email').": " . $form_state['values']['email_user'] . " ".t('already been registered').". ".t('Please change this field.'));

   $hay_domain = db_query("SELECT count(*) FROM `node__field_domain` WHERE field_domain_value='" . $form_state['values']['domain'] . "';")->fetchField();
   if($hay_domain > 0 )   
      form_set_error('Error',  $form_state, t('The Domain')." : " . $form_state['values']['domain'] . " ".t('already been registered').". ".t('Please change this field.'));

 }


  function submitForm(array &$form, array &$form_state) {

/*
*  Information for Persona data, Put on data type values of form
*
*/


/*
*   Instancia para crear un contenido de tipo sucursal
*/
    $F_sucursal = array(
      'title' => $form_state['input']['branch'],
      'field_codigo' => $this->newCodPersonBranch('suc'),
      'field_telefono' => $form_state['input']['phonebranch'],
      'field_actividad_principal' => $form_state['input']['Activity'],
      'field_departamento' => $form_state['input']['department'],
      'field_direccion' => $form_state['values']['address'],
      'field_email' => $form_state['input']['email'],
      'field_dia_informe' => $form_state['input']['informationday'],
      'field_fecha_ini' => $form_state['input']['datebegin'],
      'field_matricula_comercial' => $form_state['input']['c_registration'],
      'field_moneda' => $form_state['input']['currency'],
      'field_segunda_moneda' => NULL,
      'field_nit' => $form_state['input']['nit'],
      'field_nro_empleados' => $form_state['input']['nroemployees'],
      'field_nro_sucursales' => $form_state['input']['nrobranches'],
      'field_pais' => $form_state['input']['country'],
      'field_rubro' => $form_state['input']['Entry'],
      'field_sucursal' => $form_state['input']['branch'],
      'field_domain' => $form_state['input']['domain'],
      'type' => 'sucursal',
    );

/*
*   Instancia para crear un usuario
*/
    $txt=$form_state['input']['domain'];
    $clave=strtoupper($txt[0]).$txt[1]."us".rand(1000,10000);


    $F_user = array(
      'name' => $form_state['values']['email_user'],
      'mail' => $form_state['values']['email_user'],
      'pass' => $clave,
//      'pass' => $form_state['values']['re_password'],
    );


    $usuario = $this->drupalCreateUser($F_user);

/*
*   Instancia para crear un contenido de tipo persona
*/

    $F_persona = array(
      'title' => $form_state['values']['name'] . " " . $form_state['values']['lastName'],
      'field_codigo' => $this->newCodPersonBranch(),
      'field_nombres' => $form_state['values']['name'],
      'field_apellidos' => $form_state['values']['lastName'],
      'field_telefono' => $form_state['values']['phone'],
      'field_usuarioid' => $usuario->id(),
      'type' => 'persona',
    );

/*
*   Instancia para crear un contenido de tipo sub domain
*/
    $db = str_replace("-","",$form_state['values']['domain']);
    $db = str_replace("_","",$db);

    $subdomain = $form_state['values']['domain'];
    $db_subdomain = $db.$form_state['values']['db_domain'];
    $db_user = $form_state['values']['us_domain'];
    $db_password = $form_state['values']['pass_domain'];



    $F_subdomain = array(
      'title' => $subdomain ,
      'field_data_base' => $db_subdomain ,
      'field_user' => $db_user ,
      'field_password' => $db_password ,
      'type' => 'sub_domain',
    );

/*
*   Crear instacias de la entidad Node de un tipo de dato especifico
*/
    $nodoPer = $this->drupalCreateNode($F_persona);
    $nodoSuc = $this->drupalCreateNode($F_sucursal);
    $nodoSdom = $this->drupalCreateNode($F_subdomain);
   
    db_insert('k_person_branch')->fields(array('pid' => $nodoPer->id(), 'sid' => $nodoSuc->id() ))->execute();
    $res=0;
    $uid=1;

/*    $newres = db_query("select max(uid) from k_usuario")->fetchField();
    if($res > 0){
      $uid = $res + 1;
    }
    
    db_insert('usuario')->fields(array('uid' => $uid, 'login' => $form_state['values']['email_user'], 'password' => $clave, 'email' => $form_state['values']['email_user']  ))->execute();
*/
    $newSubdomain = new creator($subdomain , $db_subdomain ,$db_user , $db_password,  $form_state['values']['email_user'], $clave);
    $newSubdomain->creator_all();
    $mailuax = $this->send_email($subdomain, $form_state['values']['email_user'], $clave, $form_state['values']['email_user'], $uid);
//    drupal_set_message("Drakosoft sub-domain was created, the income data were sent to their email dsfsd@dssdf.com");
    drupal_set_message(t('Your account has been successfully registered. Thanks for trusting Khipu').'<br>'.t('Your login details were sent to their email').' : '.$form_state['values']['email_user'] );
//    $form_state['redirect'] = 'http://www.'.$subdomain.'.khipu.com.bo';
    return;  
  }




/* ********************************************************************/
  /**
   * Creates a node based on default settings.
   *
   * @param array $settings
   *   (optional) An associative array of settings for the node, as used in
   *   entity_create(). Override the defaults by specifying the key and value
   *   in the array, for example:
   *   @code
   *     $this->drupalCreateNode(array(
   *       'title' => t('Hello, world!'),
   *       'type' => 'article',
   *     ));
   *   @endcode
   *   The following defaults are provided:
   *   - body: Random string using the default filter format:
   *     @code
   *       $settings['body'][0] = array(
   *         'value' => $this->randomName(32),
   *         'format' => filter_default_format(),
   *       );
   *     @endcode
   *   - title: Random string.
   *   - comment: COMMENT_OPEN.
   *   - changed: REQUEST_TIME.
   *   - promote: NODE_NOT_PROMOTED.
   *   - log: Empty string.
   *   - status: NODE_PUBLISHED.
   *   - sticky: NODE_NOT_STICKY.
   *   - type: 'page'.
   *   - langcode: Language::LANGCODE_NOT_SPECIFIED.
   *   - uid: The currently logged in user, or the user running test.
   *   - revision: 1. (Backwards-compatible binary flag indicating whether a
   *     new revision should be created; use 1 to specify a new revision.)
   *
   * @return \Drupal\node\Entity\Node
   *   The created node entity.
   */
  protected function drupalCreateNode(array $settings = array()) {

    $settings += array(
      'body'      => array(array()),
      'changed'   => REQUEST_TIME,
      'promote'   => NODE_NOT_PROMOTED,
      'revision'  => 1,
      'log'       => '',
      'status'    => NODE_PUBLISHED,
      'sticky'    => NODE_NOT_STICKY,
    );
    if (isset($settings['created']) && !isset($settings['date'])) {
      $settings['date'] = format_date($settings['created'], 'custom', 'Y-m-d H:i:s O');
    }
    // If the node's user uid is not specified manually, use the currently
    // logged in user if available, or else the user running the test.
    if (!isset($settings['uid'])) {
     if ($this->loggedInUser) {
       $settings['uid'] = 1;//$this->loggedInUser->id();
    }
      else {
        $user = \Drupal::currentUser();
        $user = \Drupal::currentUser() ?: $GLOBALS['user'];
        $settings['uid'] = $user->id();
      }
    }

    $node = entity_create('node', $settings);
    if (!empty($settings['revision'])) {
      $node->setNewRevision();
    }
    $node->save();
    return $node;
  }

  protected function drupalCreateUser(array $edit = array(), $us = NULL) {
    $edit += array(
      'status' => 1,
      'roles'  => array('Authenticated user', 'cliente'),
    );

    $account = entity_create('user', $edit);
    $account->save();

    if (!$account->id()) {
      return FALSE;
    }
    // Add the raw password so that we can log in as this user.
    $account->pass_raw = $edit['pass'];
    return $account;
  }


  public function send_email($domain, $usu, $cla, $mail, $idusuax){
/*  $body = '<p><img src="http://www.khipu.com.bo/sites/all/themes/khipu2/logo.png" alt="Khipu"></p><p><b>'.t('CONFIRMATION OF KHIPU ACCOUNT').'</b></p>
           <p>'.t('Thanks for registering for the free version of <b>khipu</b> your access data for system are:').'<br>
           <b>'.t('USER').':</b> '.$usu.'<br><b>'.t('PASSWORD').':</b> '.$cla.'</p><p>'.t('to confirm your account click here').': 
           <a href="http://'.$domain.'.khipu.com.bo/welcome/'.$idusuax.'">'.$domain.'.khipu.com.bo/welcome/'.$idusuax.'</a></p>';
*/
  $body = '<p><img src="http://sistema.khipu.com.bo/themes/khipuman/Logo-slogan.png" alt="Khipu"></p>
           <p><b>'.t('CONFIRMATION OF YOUR ACCOUNT KHIPU'). " | ". t('FREE VERSION').'</b></p>
           <p>'.t('Thanks for registering for the free version of <b>khipu</b> your access data for system are').':<br>
           <b>'.t('USER').':</b> '.$usu.'<br><b>'.t('PASSWORD').':</b> '.$cla.'</p><p>'.t('to confirm your account click here').': 
           <a href="http://'.$domain.'.khipu.com.bo/welcome/'.$idusuax.'">'.$domain.'.khipu.com.bo/welcome/'.$idusuax.'</a></p>';

 
   $params['body'] = $body;
      $sent = drupal_mail('clientregister', 'create_domain', $mail, language_default(), $params, "info@khipu.com.bo", TRUE); //info@khipu.com.bo
    if(!$sent){
      drupal_set_message("falla en el envio");
   }
   return TRUE;    
}

}