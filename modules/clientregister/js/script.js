/**
     * @file
     * Defines Javascript behaviors for the node module.
     */
(function(a){a.fn.validCampo=function(b){a(this).on({keypress:function(a){
	var c=a.which,d=a.keyCode, e=String.fromCharCode(c).toLowerCase(), f=b;(-1!=f.indexOf(e)||9==d||37!=c&&37==d||39==d&&39!=c||8==d||46==d&&46!=c)&&161!=c||a.preventDefault()}})}})(jQuery);
 
 var normalize = (function() {
  var from = " ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç.#'\\ºª!|$%&()=?¿¡[];,:{}+<>€¬~@/",
      to   = "-AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }
      return ret.join( '' );
  }
 
})();
 
 
(function ($) {
  $(document).ready(function(){
/*
*  Validaciones de campos
*/		
		$('#edit-phone').validCampo('0123456789-');
		$('#edit-phonebranch').validCampo('0123456789-');
		$('#edit-nroemployees').validCampo('0123456789');
		$('#edit-nrobranches').validCampo('0123456789');
		$('#edit-c-registration').validCampo('0123456789');
		$('#edit-nit').validCampo('0123456789');
  	$('#edit-domain').validCampo('abcdefghijklmnopqrstuvwxyz-_0123456789');
    $("#edit-re-password-pass1").attr("placeholder", "_Clave");
    $("#edit-re-password-pass2").attr("placeholder", "_Confirmar clave");

/*
* Controla la fecha cada cuanto se pagara a la renta
*/
   $("#edit-nit").keyup(function() {
      dato = this.value
      dig = parseInt(dato[dato.length-1]) + 13
      $('#edit-informationday').val(dig)
    }); 
		
   $("#edit-submit").attr("disabled", true);
   $("#edit-submit").addClass("disabled");
   
   
/***************   BOTON NEXT DEL DATA PERSON   ***************/

   $("#dataPerson .next a" ).click(function() {
      $("#dataCompany").css('display','inherit')
      $("#dataPerson").css('display','none')
      $("#navegation li.step1").removeClass("active");
      $("#navegation li.step2").addClass("active");
      return false;
   }); 

/***************   BOTON NEXT AND BACK DEL DATA COMPANY   ***************/
    $("#dataCompany .next a" ).click(function() {
      var dato = $("#edit-branch").val().toLowerCase();
      dato = normalize(dato);
	  dato = dato.substring(0, 20)
		  $("#edit-domain").val(dato);

      $("#dataTributary").css('display','inherit')
      $("#dataCompany").css('display','none')
      $("#navegation li.step2").removeClass("active");
      $("#navegation li.step3").addClass("active");
     return false;
    }); 

    $("#dataCompany .back a" ).click(function() {
      $("#dataPerson").css('display','inherit')
      $("#dataCompany").css('display','none')
      $("#navegation li.step2").removeClass("active");
      $("#navegation li.step1").addClass("active");
      return false;
    }); 

/***************   BOTON NEXT AND BACK DEL DATA TRIBUTARY   ***************/
    $("#dataTributary .next a" ).click(function() {
      $("#dataUser").css('display','inherit')
      $("#dataTributary").css('display','none')
      $("#edit-submit").css('display','inherit')
      $("#navegation li.step3").removeClass("active");
      $("#navegation li.step4").addClass("active");
      return false;
    }); 

    $("#dataTributary .back a" ).click(function() {
      $("#dataCompany").css('display','inherit')
      $("#dataTributary").css('display','none')
      $("#navegation li.step3").removeClass("active");
      $("#navegation li.step2").addClass("active");
      return false;
    }); 

/***************   BOTON NEXT AND BACK DEL DATA USER   ***************/
    $("#dataUser .back a" ).click(function() {
      $("#dataTributary").css('display','inherit')
      $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
      $("#navegation li.step4").removeClass("active");
      $("#navegation li.step3").addClass("active");
      return false;
    }); 

/***************   BOTONES DE NAVEGACION   ***************/

    $("#navegation .step1 a" ).click(function() {
	   $("#navegation li.step1").addClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','inherit')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','none')
     $("#edit-submit").css('display','none')
     return false;
    // return false;
    }); 
    $("#navegation .step2 a" ).click(function() {
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").addClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','inherit')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
     return false;
    }); 
    $("#navegation .step3 a" ).click(function() {
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").addClass("active");
     $("#navegation li.step4").removeClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','inherit')
     $("#dataUser").css('display','none')
      $("#edit-submit").css('display','none')
     return false;
    }); 

    $("#navegation .step4 a" ).click(function() {
     var dato = $("#edit-branch").val().toLowerCase();
     dato = normalize(dato);
     dato = dato.substring(0, 20)
	 $("#edit-domain").val(dato);
     $("#navegation li.step1").removeClass("active");
     $("#navegation li.step2").removeClass("active");
     $("#navegation li.step3").removeClass("active");
     $("#navegation li.step4").addClass("active");

     $("#dataPerson").css('display','none')
     $("#dataCompany").css('display','none')
     $("#dataTributary").css('display','none')
     $("#dataUser").css('display','inherit')
      $("#edit-submit").css('display','inherit')
     return false;
    }); 
  /* 
  *
  */

   if($("#edit-newsletter-0").is(":checked")) {
      $("#edit-submit").attr("disabled", false);
     $("#edit-submit").removeClass("disabled");
   }


   $("#edit-newsletter-0").click(function() {
    if($(this).is(":checked")) {
      $("#edit-submit").attr("disabled", false);
     $("#edit-submit").removeClass("disabled");
   }else{
     $("#edit-submit").attr("disabled", true);
     $("#edit-submit").addClass("disabled");
   }   
   });


   $("#edit-submit").click(function() {
	 //  men = "Completa este campo :";
			
	   
		 /*
		 *  Campos del bloque "Informacion de negocios"
		 */

	   if($("#edit-branch").val().length == 0  || $("#edit-phonebranch").val().length == 0  || $("#edit-email").val().length == 0  || $("#edit-country").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }

	   if($("#edit-department").val().length == 0  || $("#edit-address").val().length == 0  || $("#edit-activity").val().length == 0  || $("#edit-entry").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }
     
	   if($("#edit-nroemployees").val().length == 0  || $("#edit-nrobranches").val().length == 0  || $("#edit-currency").val().length == 0 ) 
	   { $("#navegation .step2 a").trigger('click'); }

		 /*
		 *  Campos del bloque "Informacion personal"
		 */
		 
		 if($("#edit-name").val().length == 0  || $("#edit-lastname").val().length == 0  )
	   { $("#navegation .step1 a").trigger('click'); }
	   


	   if($("#edit-branch").val().length > 0  && $("#edit-phonebranch").val().length > 0  && $("#edit-email").val().length > 0  && $("#edit-country").val().length > 0 ) 
	   { 
       if($("#edit-department").val().length > 0  && $("#edit-address").val().length > 0  && $("#edit-activity").val().length > 0  && $("#edit-entry").val().length > 0 ) 
	   { 
	   if($("#edit-nroemployees").val().length > 0  && $("#edit-nrobranches").val().length > 0  && $("#edit-currency").val().length > 0 ) 
	   {  
	   if($("#edit-name").val().length > 0  && $("#edit-lastname").val().length > 0   && $("#edit-email-user").val().length > 0 ) // edit-email-user
	   { 
	      $("#loader").css('display', 'inherit');
	   }}}}



   });
/* ******************************************
   popup para ver politicas y servicios
  *********************************************/
//     $( "#politicas" ).dialog({ autoOpen: false });
	$(".police a").click(function() {
		$("#popup").css('display', 'inherit')
			return false;
		});
	});
   
   
   
})(jQuery);
