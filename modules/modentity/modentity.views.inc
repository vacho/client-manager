<?php

/*
 * @file
 * Provide views data for modentity.module.
 */


/**
 *  Implements hook_views_data.
 */

function modentity_views_data() {
  $data['modentity']['table']['group']= t('Module entity');

  $data['modentity']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Table for modules entities'),
    'help' => t('whatever you are looking for'),
    'weight' => -10,
  );

  $data['modentity']['table']['entity type'] = 'modentity';

  $data['modentity']['mid'] =array(
    'title' => 'mid',
    'help' => t('The id of Module entity'),
    'field' => array(
      'id' => 'standard',
    ),
  );

  $data['modentity']['name'] = array(
    'title' => t('Name of the Module entity'),
    'help'=> t('Name of the Module entity'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['modentity']['type'] = array(
    'title' => t('Entity type'),
    'filter' => array(
      'id' => 'string',
    ),
  );
  $data['modentity']['activado'] = array(
    'title' => t('Module entity active'),
    'help'=> t('Module entity active'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );
  return $data;
}
