<?php

/**
 * @file
 * Contains \Drupal\galli\GalliViewBuilder.
 */

namespace Drupal\modentity;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
/**
 * Defines an entity view builder for a galli entity.
 *
 * @see \Drupal\galli
 */
class ModentityViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildContent(array $entities, array $displays, $view_mode, $langcode = NULL) {
    parent::buildContent($entities, $displays, $view_mode, $langcode);

    foreach ($entities as $entity) {
       $entity->content['modentity']['name'] = array(
        '#markup' => $entity->getName(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
      $entity->content['modentity']['activado'] = array(
        '#markup' => $entity->getActivado(),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );

    }
  }
}
