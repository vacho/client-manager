<?php
/**
 * @file
 * Definition of Drupal\modentity\ModentityFormController.
 */

namespace Drupal\modentity;

use Drupal\Core\Entity\ContentEntityForm;

use Drupal\Core\Language\Language;

/**
 * Form controller for the Module entity edit forms.
 */
class ModentityFormController extends ContentEntityForm {

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name').' ('.t('Module').')',
      '#default_value' => $entity->name->value,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => -10,
    );

/*    $form['user_id'] = array(
      '#type' => 'textfield',
      '#title' => 'UID',
      '#default_value' => $entity->user_id->target_id,
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => -10,
    );
*/    $form['activado'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#default_value' => $entity->activado->value,
    );

/*    $form['langcode'] = array(
      '#title' => t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->id,
      '#languages' => Language::STATE_ALL,
    );

*/
    return $form;
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::submit().
   */
  public function submit(array $form, array &$form_state) {
    // Build the entity object from the submitted values.
	$form['type'] = 'modentity';
    $entity = parent::submit($form, $form_state);
    $form_state['redirect'] = 'modentity-entities';

    return $entity;
  }


  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $entity = $this->entity;
    $entity->save();
  }
}

