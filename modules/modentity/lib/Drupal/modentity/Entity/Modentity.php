<?php
/**
 * @file
 * Definition of Drupal\package\Entity\package.
 */

namespace Drupal\modentity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Package entity class.
 *
 * @ContentEntityType(
 *   id = "modentity",
 *   label = @Translation("Modules entity"),
 *   controllers = {
 *     "view_builder" = "Drupal\modentity\ModentityViewBuilder",
 *     "form" = {
 *       "edit" = "Drupal\modentity\ModentityFormController",
 *       "add" = "Drupal\modentity\ModentityFormController",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationController"
 *   },
 *   base_table = "modentity",
 *   admin_permission = "administer display modes",
 *   fieldable = TRUE,
 *   field_cache = FALSE,
 *   entity_keys = {
 *     "id" = "mid",
 *     "uuid" = "uuid",
 *     "label" = "name"
 *   },
 * )
 */
class Modentity extends ContentEntityBase {

  public function id() {
    return $this->get('mid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivado() {
    return $this->activado->value;
  }


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['mid'] = FieldDefinition::create('integer')
      ->setLabel('ID')
      ->setDescription(t('The ID of the Module entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = FieldDefinition::create('uuid')
      ->setLabel('UUID')
      ->setDescription(t('The UUID of the Module entity.'))
      ->setReadOnly(TRUE);

    $fields['langcode'] = FieldDefinition::create('language')
      ->setLabel(t('Language').' '.t('code'))
      ->setDescription(t('The language code of the Module entity.'));

    $fields['name'] = FieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the module entity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    // @todo: Add allowed values validation.
    $fields['type'] = FieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The bundle of the module entity.'))
      ->setRequired(TRUE);

    $fields['activado'] = FieldDefinition::create('string')
      ->setLabel(t('Module active'))
      ->setDescription(t('The active module entity.'))
      ->setTranslatable(TRUE)
      ->setPropertyConstraints('value', array('Length' => array('max' => 32)));

    return $fields;
  }

}




