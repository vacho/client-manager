MODIFICACIONES QUE SE DEBEN DE REALIZAR
------------------------------
1.- En el archivo lib/Drupal/creator/creator.php, modificar los siguiente:
  - en la funcion __construct
    - $this->account :  por la cuenta del sitio a utilizar
	- $this->domain  :  por el dominio a utilizar
	- $this->user
	- $this->password


	ANTES DE INSTALAR CREATOR 
------------------------------
1.- Crear un drupal 8 base con el nombre de drupaldev
2.- Instalar el modulo create user en el nuevo drupaldev
3.- Sacar un backup de la base de datos en formato .sql
4.- Renombrar el archivo quenerado por drupaldev.sql
5.- Copiar este archivo "drupaldev.sql" al modulo creator dentro la carpeta files (Remplazar el existente).
