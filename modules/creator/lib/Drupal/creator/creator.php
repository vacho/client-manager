<?php
/**
*  @file 
*   Contains Drupal\creator.
*/


namespace Drupal\creator;
use Drupal\creator\xmlapi;

class creator
{  
  /*
  * Permite la creación de un sitio completo Drupal en un subdominio llamando a la función creator_all
  */
  
  private $subDomain;
  private $db;
  private $dbuser;
  private $dbpass;
  private $dbnewuser;
  private $dbnewpass;
  
  private $domain;
  private $account;
  private $ip;
  private $user;
  private $password;
  
  private $srcFiles;
  private $src;
  private $dst;
  
  private $fileName;
  
  /*
   * Constructor
  */ 
public function __construct($subD ='', $dbN ='', $dbU ='', $dbUP ='', $dbNewUs='', $dbNewPass='' ){
    
    $this->subDomain = $subD;
    
    $this->domain = "khipu.com.bo";
    $this->account = "khipucom";
    $this->ip = $_SERVER['SERVER_ADDR'];
    $this->user = "khipucom";
    $this->password = "kamayuC578";
    
    $this->srcFiles = "drupaldev";
    $this->src = "/home/".$this -> account."/public_html/".$this -> srcFiles."/";
    $this->dst = "/home/".$this -> account."/public_html/".$subD."/";
    
    $this -> fileName = $_SERVER['DOCUMENT_ROOT'].'/modules/creator/files/khipucom_drupaldev.sql';
    
    $this->db = $this->account."_".$dbN;
    $this->dbuser = $this->account."_".$dbU;
    $this->dbpass = $dbUP;
    $this->dbnewuser = $dbNewUs;
    $this->dbnewpass = $dbNewPass;
  }  
  //Funciones get
  public function getSubDomain(){ return $this->subDomain;}
  
  public function getDb(){ return $this->db;}
  
  public function getDbuser(){ return $this->dbuser;}
  
  public function getDbpass(){ return $this->dbpass;}
  
  public function getDomain(){ return $this->domain;}
  
  public function getAccount(){ return $this->account;}
  
  public function getUser(){ return $this->user;}
  
  public function getPassword(){ return $this->password;}
  
  public function getSrcFiles(){ return $this->srcFiles;}
  
  public function getSrc(){ return $this->src;}
  
  public function getDst(){ return $this->dst;}
  
  public function getFileName(){ return $this->fileName;}
  
  //funciones set
  
  public function setSubDomain($sb){ $this->subDomain = $sb;}
  
  public function setDb($d){ $this->db = $d;}
  
  public function setDbuser($dbu){ $this->dbuser = $dbu;}
  
  public function setDbpass($dbp){ $this->dbpass = $dbp;}
  
  public function setDomain($do){ $this->domain = $do;}
  
  public function setAccount($a){ $this->account = $a;}
  
  public function setUser($u){ $this->user = $u;}
  
  public function setPassword($p){ $this->password = $p;}
  
  public function setSrcFiles($sf){ $this->srcFiles = $sf;}
  
  public function setSrc($s){ $this->src = $s;}
  
  public function setDst($ds){ $this->dst = $ds;}
  
  public function setFileName($fn){ $this->fileName = $fn;}
    
  /*
  * create_suddomain es una función que crea un suddominio usando xmlapi para conectarse al cpanel
  */
  public function create_subdomain(){
   // include 'xmlapi.php';
    //global $ip,$user,$password,$subDomain,$domain;
    $xmlapi = new xmlapi($this->ip);
    $xmlapi->password_auth($this->user,$this->password);
    $xmlapi->set_output('json');
 
    $xmlapi->set_debug(1);
    $xmlapi->api1_query($this->user, "SubDomain", "addsubdomain", array($this->subDomain.'.'.$this->domain, $this->domain, 0, 0, 'public_html/'.$this->subDomain.'/') );
  }

  /*
  * copy_files es una función que copia los archivos de un drupal a un destino definido
  */
  public function copy_files($src,$dst) {

    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
      if (( $file != '.' ) && ( $file != '..' )) {
        if ( is_dir($src . '/' . $file) ) {
          $this->copy_files($src . '/' . $file,$dst . '/' . $file);
        }
      else {
        copy($src . '/' . $file,$dst . '/' . $file);
        }
      }
    }
    closedir($dir);
  } 

  /*
  * data_base es una función que crea bd y un usuario, asigna privilegios a el usaurio creado y llena la bd drupal 
  */
  public function data_base(){
    //global $user,$password,$db,$dbuser,$dbpass,$fileName;
    
    $link = mysql_connect('localhost', $this->user, $this->password);
  
    if (!$link) {
      die('No pudo conectarse: ' . mysql_error());
    }
/*    else {
      print "conexión exitosa a la base de datos<br>";
    }
 */   // creamos base de datos
    $sql = 'CREATE DATABASE '.$this->db;
    if (mysql_query($sql, $link)) {
//      print "La base de datos fue creada satisfactoriamente<br>";
      $aux='';
    }
    else {
      die('Error al crear la base de datos: ' . mysql_error() . "\n");
    }
    // Creamos usuario
    $sql = "CREATE USER '".$this->dbuser."'@'localhost' IDENTIFIED BY '".$this->dbpass."'";
    if (mysql_query($sql, $link)) {
//      print "Usuario creado satisfactoriamente<br>";
      $aux="";
    }
    else {
      die('Error al crear usuario: ' . mysql_error() . "\n");
    }
  
    // Asignamos privilegios al usuario sobre la base de datos creada
    $sql = "GRANT ALL PRIVILEGES ON ".$this->db.".* TO '".$this->account."'@'localhost' WITH GRANT OPTION;";
    if (mysql_query($sql, $link)) {
     // print "Se asigno con exito los privilegios al usurio sobre la base de datos<br>";
     $aux="";
    }
    else {
      die('Error al asignar privilegio a la bd y el usuario: ' . mysql_error() . "\n");
    }
  
    // Seleccionando la bd
    $bd_select = mysql_select_db($this->db, $link);
    if (!$bd_select) {
      die ('No se puede usar bd : ' . mysql_error());
    }
/*    else{ 
      print "La base de datos se selecciono satisfactoriamente<br>";
    }
 */ 
    // Asignamos privilegios a usuario
    $privileges = mysql_query('GRANT ALL PRIVILEGES ON '.$this->db.'.*  TO '.$this->dbuser.'@"%" IDENTIFIED BY "'.$this->dbpass.'" WITH GRANT OPTION;');
    if (!$privileges) {
      die('No se asignaron los privilegios: ' . mysql_error());
    }
/*    else {
      print 'Se asignaron los privilegios satisfactoriamente<br>';
    }  */
    $privileges = mysql_query('FLUSH PRIVILEGES');
    if (!$privileges) {
      die('No se recargaron los privilegios: ' . mysql_error());
    }
  /*  else{
      print 'Se recargaron privilegios satisfactoriamente<br>';
    }
  */
    //cargar la BD desde archivo 
    //NOTA: no deben haber ; que corten sentencias.
//    $sqlfile = explode(";",file_get_contents($this->fileName));  RAMIRO
    $sqlfile = explode(";",$this->deployedSQL());
    foreach($sqlfile as $query){
      mysql_query($query,$link);
    }
    $sql="insert into k_usuario values(1, '".$this->dbnewuser."', '".$this->dbnewpass."', '".$this->dbnewuser."');";
    mysql_query($sql,$link);
  }

  /*
  * config_settings es una función que configura el archivo settings.php de drupal para ser usado por su usuario de base de datos
  */

  public function config_settings(){
     $fileSettings="/home/".$this->account."/public_html/".$this->subDomain."/sites/default/settings.php";
		 $this->settingSQLWrite($fileSettings,$this->settingSQLRead($fileSettings));
	}
	
/*	
  public function config_settings(){
    //global $user,$password,$subDomain,$account,$db,$dbuser,$dbpass;
  
    $fileSettings="/home/".$this->account."/public_html/".$this->subDomain."/sites/default/settings.php";
    $contentFile=file_get_contents($fileSettings);
  
    //bd
    $changeBd=str_replace("'database' => 'bddrupal8a11',", "'database' => '".$this->db."',", $contentFile);
    $gestor = fopen($fileSettings, "w");
    if(fwrite($gestor, $changeBd))
//      print "Se configuro la bd<br>";
      $a="";
    else
      print "No se pudo escribir en el fichero<br>";
    @fclose($fileSettings);  

    //usuario
    $contentFile=file_get_contents($fileSettings);
    $changeUser=str_replace("'username' => 'root',", "'username' => '".$this->dbuser."',", $contentFile);
    $gestor = fopen($fileSettings, "w");
    if(fwrite($gestor, $changeUser))
     // echo "Se configuro el usuario de la BD<br>";
     $a="";
    else
      echo "No se pudo escribir en el fichero";
    @fclose($fileSettings);  

    //password
    $contentFile=file_get_contents($fileSettings);
    $changeUserPass=str_replace("'password' => 'root',", "'password' => '".$this->dbpass."',", $contentFile);
    $gestor = fopen($fileSettings, "w");
    if(fwrite($gestor, $changeUserPass))
      //echo "Se configuro password de usuario de la BD<br>";
      $a="";
    else
      echo "No se pudo escribir en el fichero";
    @fclose($fileSettings);
  }

  /*
  * creator_all es una función que llama a todas las funciones anteriores
  */
  public function creator_all(){
//    global $src,$dst;
    $this->create_subdomain();
    $this->copy_files($this->src,$this->dst);
    $this->data_base();
    $this->config_settings();
  }


  /*
  * settingSQLWrite es una función que sobre-escribe el contenido de un archivo
  */

  public function settingSQLWrite($filesetting, $txt){
  if (!$file = fopen($filesetting, 'w')) {
    echo "No se puede abrir el archivo ($filesetting)";
    exit;
  }
	fwrite($file, $txt);
	@fclose($file);
}

  /*
  * settingSQLRead, cambia los parametros de conexion del setting
  */

  public function settingSQLRead($filesetting){

  if (!$file = fopen ($filesetting, "r")) {
    echo "Imposible abrir el archivo remoto.";
    exit;
  }
	$txt="";
	$line="";
  while (!feof ($file)) {
    $line = fgets ($file, 1024);
    if (preg_match ("/'database' => '/i", $line)) {
			$line = "  'database' => '".$this->db."',".PHP_EOL;
		}
    if (preg_match ("/'username' => '/i", $line)) {
			$line = "  'username' => '".$this->dbuser."',".PHP_EOL;
		}
    if (preg_match ("/'password' => '/i", $line)) {
			$line = "  'password' => '".$this->dbpass."',".PHP_EOL;
		}
		$txt .= $line;
	}
	@fclose($file);
	return $txt;
}


  
public function deployedSQL(){
  $consulta="";
  $file = fopen ($this->fileName, "r");
  if (!$file) {
    echo "<p>Imposible abrir el archivo remoto.\n";
    exit;
  }
  $b_Ins = 0;
  while (!feof ($file)) {
    $line = fgets ($file, 1024);
    $pos="";
    if(strlen($line)>1){
      $pos = $line[strlen($line)-2];
    }
    if($b_Ins == 3){
      $b_Ins = -1;
    }
/*
*  Limpia los inserts en los caches
*/  
    if (preg_match ("/INSERT INTO `cache_/i", $line)) {
       $b_Ins = 1;
    }
    if($b_Ins >= 1 && $pos != ";"){
      $b_Ins = 2;
    }
    if($b_Ins >= 1 && $pos == ";"){
      $b_Ins = 3;
    }
/*
*  remplaza los ; dentro de una instrucción sql 
*/
    if($b_Ins < 1 ){
      if (preg_match ("/;/i", $line)) {
          $line=str_replace(";",",",$line);
          if($pos == ";"){
             $line[strlen($line)-2] = ";";
          }
      }
     $consulta .= $line; 
    }
  }
  fclose($file);
  return $consulta;
}
  
  

}